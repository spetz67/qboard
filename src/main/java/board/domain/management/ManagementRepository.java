package board.domain.management;

import javax.validation.Valid;
import java.util.stream.Stream;

public interface ManagementRepository<T>
{
    Stream<T> getAll();

    void deleteAll();

    void saveAll(@Valid Stream<T> entities);
}
