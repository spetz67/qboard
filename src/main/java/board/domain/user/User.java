package board.domain.user;

import board.domain.common.EventQueue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@EqualsAndHashCode
@ToString
public class User
{
    @Getter
    @EqualsAndHashCode.Exclude
    @JsonIgnore
    private final EventQueue eventQueue = new EventQueue();

    @Getter
    @NotNull
    private final UUID id;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    @Min(1)
    private Integer revisionNumber;

    @Getter
    @NotNull
    private final Instant createdInstant;

    @Getter
    @Setter(AccessLevel.PACKAGE)
    private Instant updatedInstant;

    @NotEmpty
    @Getter
    private String name;

    @Size(max = 60) // as defined in database
    @Getter
    @Setter
    private String notificationEmailAddress;

    @Min(0)
    @Getter
    private int totalWordCount;

    @Getter
    @Setter
    @NotNull
    private List<PhoneContact> phoneContacts;

    @Getter
    @Setter
    @NotNull
    private List<EmailContact> emailContacts;


    public User(UUID id, Integer revisionNumber, Instant createdInstant, Instant updatedInstant, @NotEmpty String name,
            String notificationEmailAddress, int totalWordCount, List<PhoneContact> phoneContacts,
            List<EmailContact> emailContacts)
    {
        this.id = id;
        this.revisionNumber = revisionNumber;
        this.createdInstant = createdInstant;
        this.updatedInstant = updatedInstant;
        this.name = name;
        this.notificationEmailAddress = notificationEmailAddress;
        this.totalWordCount = totalWordCount;
        this.phoneContacts = phoneContacts;
        this.emailContacts = emailContacts;
    }

    void addWordCount(int wordCount)
    {
        totalWordCount += wordCount;
    }

    void subtractWordCount(int wordCount)
    {
        totalWordCount -= wordCount;
    }

    public void setName(String name)
    {
        if (!Objects.equals(this.name, name))
        {
            this.name = name;
            eventQueue.add(new UserNameChangedEvent(this));
        }
    }
}
