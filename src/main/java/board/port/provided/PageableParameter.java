package board.port.provided;

import board.application.Pageable;
import lombok.Data;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class PageableParameter
{
    @QueryParam("page")
    @DefaultValue("0")
    @Parameter(description = "Requested page number, starting with 0")
    private int page;

    @QueryParam("size")
    @DefaultValue("100")
    @Parameter(description = "Requested page size")
    private int size;

    @QueryParam("sort")
    @Parameter(description = "Sort property names, optionally followed by :asc or :desc")
    private List<String> sort;

    public Pageable toPageable()
    {
        List<Pageable.Order> orderBy = getSort()!=null && !getSort().isEmpty()
                ? getSort().stream().map(this::sortToOrder).collect(Collectors.toList())
                : List.of();
        return new Pageable(getSize(), getPage(), orderBy);
    }

    private Pageable.Order sortToOrder(String sort)
    {
        int separatorIndex = sort.indexOf(':');
        if (separatorIndex<0)
            return new Pageable.Order(sort, true);
        String direction = sort.substring(separatorIndex+1);
        if (direction.equalsIgnoreCase("asc"))
            return new Pageable.Order(sort.substring(0, separatorIndex), true);
        if (direction.equalsIgnoreCase("desc"))
            return new Pageable.Order(sort.substring(0, separatorIndex), false);
        throw new ClientErrorException("Invalid sort parameter", Response.Status.BAD_REQUEST);
    }
}
