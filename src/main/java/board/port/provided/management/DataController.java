package board.port.provided.management;

import board.application.management.DataService;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Path("/management/data")
@Produces(MediaType.APPLICATION_JSON)
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Management")
@RequiredArgsConstructor
public class DataController
{
    private final DataService dataService;

    @GET
    @Produces("application/zip")
    public Response exportData()
    {
        Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);
        String downloadFileName = "board-data#%s.zip".formatted(now);
        return Response
                .ok((StreamingOutput) dataService::exportAsZip)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename="+downloadFileName)
                .build();
    }

    @PUT
    @Consumes("application/zip")
    public void importData(InputStream input) throws IOException
    {
        dataService.importFromZip(input);
    }
}