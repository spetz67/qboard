package board.port.required.management;

import board.domain.management.RepositoryLock;
import board.port.required.CacheKeys;
import io.quarkus.cache.CacheInvalidateAll;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;

@Singleton
@RequiredArgsConstructor
class RepositoryLockImpl implements RepositoryLock
{
    private static final String DATA_TABLES = "\"user\",\"user_email_contact\",\"note\",\"follow_relation\",\"sequence\"";

    private final DSLContext jooq;

    @Override
    @Transactional
    public void withReadLock(Runnable runnable)
    {
        // postgres specific, todo: find better solution, e.g. with isolation level
        jooq.execute("LOCK "+DATA_TABLES+" IN SHARE MODE");
        runnable.run();
    }

    @Override
    @Transactional
    @CacheInvalidateAll(cacheName = CacheKeys.USER)
    @CacheInvalidateAll(cacheName = CacheKeys.NOTE)
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWER)
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWEE)
    public void withWriteLock(Runnable runnable)
    {
        // postgres specific
        jooq.execute("LOCK "+DATA_TABLES+" IN ACCESS EXCLUSIVE MODE");
        runnable.run();
    }
}
