package board.domain.note;

public record NotePostedEvent(Note note)
{
}
