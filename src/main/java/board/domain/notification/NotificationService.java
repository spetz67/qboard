package board.domain.notification;

import io.quarkus.runtime.StartupEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;
import javax.inject.Singleton;
import java.util.List;

/**
 * Publish notifications to users via email.
 * The notifications are queued and then published asynchronously using transactional outbox pattern.
 */
@Singleton
@RequiredArgsConstructor
@Slf4j
class NotificationService
{
    private final NotificationRepository notificationRepository;
    private final Event<NotificationSavedEvent> eventPublisher;
    private final SendEmailService sendEmailService;

    public void publish(List<Notification> notifications)
    {
        if (!notifications.isEmpty())
        {
            notificationRepository.add(notifications);
            eventPublisher.fire(new NotificationSavedEvent());
        }
    }

    void publishUnsentNotifications(@Observes StartupEvent event)
    {
        notificationRepository
                .getAllAndLock()
                .forEach(this::sendEmail);
    }

    /**
     * Trigger asynchronous publishing after the transaction has completed successfully.
     */
    void publishNewNotifications(@Observes(during = TransactionPhase.AFTER_SUCCESS) NotificationSavedEvent event)
    {
        notificationRepository
                .getByNotLockedAndLock()
                .forEach(this::sendEmail);
    }

    private void sendEmail(Notification notification)
    {
        sendEmailService.sendEmail(notification)
                .thenApply(Notification::id)
                .whenComplete((notificationId, throwable) ->
                {
                    if (throwable!=null)
                        log.warn("Sending notification email failed", throwable);
                    else
                        notificationRepository.deleteById(notificationId);
                });
    }
}
