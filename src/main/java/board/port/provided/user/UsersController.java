package board.port.provided.user;

import board.application.user.UserCommands;
import board.application.user.UserQueryRepository;
import board.application.user.UserIn;
import board.application.user.UserOut;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.Date;
import java.util.UUID;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Users")
public class UsersController
{
    private final UserQueryRepository queries;
    private final UserCommands commands;

    /**
     * Get a page of users.
     * @return page of user resources
     */
    @GET
    public PageResource<UserOut> getUsers(@BeanParam PageableParameter pageableParameter)
    {
        return PageResource.of(queries.queryUsers(pageableParameter.toPageable()));
    }

    @GET
    @Path("/{id}")
    @APIResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = UserOut.class)))
    public Response getUserById(@PathParam("id") UUID id)
    {
        UserOut user = queries.queryUserById(id);
        return Response
                .ok(user)
                .lastModified(Date.from(user.updatedInstant()))
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @APIResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "URI of the created resource",
                    schema = @Schema(type = SchemaType.STRING, format = "uri")))
    public Response createUser(@Valid UserIn userIn, @Context UriInfo uriInfo)
    {
        UUID userId = commands.registerUser(userIn);
        URI location = uriInfo.getAbsolutePathBuilder()
                .path(String.valueOf(userId))
                .build();
        return Response.created(location).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateUser(@PathParam("id") @NotNull UUID id, @Valid UserIn userIn)
    {
        commands.updateUser(id, userIn);
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") @NotNull UUID id)
    {
        commands.deleteUser(id);
    }
}