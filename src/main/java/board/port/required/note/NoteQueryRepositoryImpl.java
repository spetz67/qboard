package board.port.required.note;

import board.application.note.NoteQueryRepository;
import board.application.Page;
import board.application.Pageable;
import board.domain.note.NoteNotFoundException;
import board.application.note.NoteOut;
import board.port.required.CacheKeys;
import board.port.required.Paginator;
import io.quarkus.cache.CacheResult;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

import static board.port.required.jooq.tables.Note.NOTE;

@RequiredArgsConstructor
@Singleton
@Transactional
class NoteQueryRepositoryImpl implements NoteQueryRepository
{
    private final DSLContext jooq;
    private final Paginator paginator;

    @Transactional
    @CacheResult(cacheName = CacheKeys.NOTE)
    @Override
    public Page<NoteOut> queryNotesByUserId(UUID userId, Integer fromNumber, Pageable pageable)
    {
        if (fromNumber==null)
            fromNumber = 1;

        if (!pageable.sorted())
            pageable = pageable.order("number");

        var query = jooq
                .select(NOTE.NUMBER, NOTE.INSTANT, NOTE.TEXT, NOTE.MEDIA_JSON)
                .from(NOTE)
                .where(NOTE.USER_ID.eq(userId).and(NOTE.NUMBER.greaterOrEqual(fromNumber)));
        return paginator
                .queryPage(query, pageable)
                .map(record -> record.into(NoteOut.class));
    }

    @CacheResult(cacheName = CacheKeys.NOTE)
    @Override
    public NoteOut queryNoteByUserIdAndNumber(UUID userId, int noteNumber)
    {
        return jooq.select(NOTE.NUMBER, NOTE.INSTANT, NOTE.TEXT, NOTE.MEDIA_JSON)
                .from(NOTE)
                .where(NOTE.USER_ID.eq(userId).and(NOTE.NUMBER.eq(noteNumber)))
                .fetchOptionalInto(NoteOut.class)
                .orElseThrow(() -> new NoteNotFoundException(noteNumber));
    }
}