package board.domain.user;

import board.domain.note.Note;
import board.domain.note.BeforeNoteDeletedEvent;
import board.domain.note.NotePostedEvent;
import lombok.RequiredArgsConstructor;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
public class NoteEventHandler
{
    private final UserRepoService userRepoService;

    void notePosted(@Observes NotePostedEvent event)
    {
        Note note = event.note();
        User user = userRepoService.getById(note.userId());
        user.addWordCount(note.computeWordCount());
        userRepoService.update(user);
    }

    void noteDeleted(@Observes BeforeNoteDeletedEvent event)
    {
        Note note = event.note();
        User user = userRepoService.getById(note.userId());
        user.subtractWordCount(note.computeWordCount());
        userRepoService.update(user);
    }
}
