package board.port.provided.auth;

import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;
import java.time.Duration;
import java.util.Set;

@Singleton
public class AccessTokenGenerator
{
    @ConfigProperty(name = "mp.jwt.verify.issuer")
    String issuer;

    public String generate(String user, Set<String> roles, Duration expiresIn)
    {
        String token = Jwt.issuer(issuer)
                .upn(user)
                .groups(roles)
                .expiresIn(expiresIn)
                .sign();
        return token;
    }
}
