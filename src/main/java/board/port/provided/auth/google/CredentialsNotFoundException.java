package board.port.provided.auth.google;

import board.domain.common.TemplatedException;

import java.util.Map;

class CredentialsNotFoundException extends TemplatedException
{
    public CredentialsNotFoundException(String emailAddress)
    {
        super("error.auth.credentialsNotFound", Map.of("emailAddress", emailAddress));
    }
}
