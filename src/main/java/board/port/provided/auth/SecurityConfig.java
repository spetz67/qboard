package board.port.provided.auth;

import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperties;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.util.List;
import java.util.Optional;

@ConfigProperties(prefix = "security")
@Getter
public class SecurityConfig
{
    @ConfigProperty(name = "intercept-url-map")
    List<InterceptUrlMapEntry> interceptUrlMap;

    @ConfigProperty(name = "login.jwt")
    LoginJwtConfig loginJwtConfig;

    String admin;

    interface InterceptUrlMapEntry
    {
        String pattern();
        String access();
        Optional<String> httpMethod();
    }

    interface LoginJwtConfig
    {
        String publicKeyLocation();
        String expectedAudience();
        String issuedBy();
    }
}
