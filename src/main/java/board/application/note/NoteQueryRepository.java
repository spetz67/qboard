package board.application.note;

import board.application.Page;
import board.application.Pageable;
import board.domain.note.NoteNotFoundException;

import java.util.UUID;

public interface NoteQueryRepository
{
    Page<NoteOut> queryNotesByUserId(UUID userId, Integer fromNumber, Pageable pageable);

    NoteOut queryNoteByUserIdAndNumber(UUID userId, int number) throws NoteNotFoundException;
}
