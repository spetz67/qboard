import {Injectable} from "@angular/core";
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {catchError} from "rxjs/operators";
import Swal from 'sweetalert2';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request).pipe(
            catchError((response: HttpErrorResponse) => {

                console.error("Communication Error", response);

                // show dialog for error message
                let message = response.message;
                if (response.error?.message)
                    message = response.error.message
                Swal.fire("Communication Error", message, "error");

                return new Observable<HttpEvent<any>>();
            })
        );
    }
}