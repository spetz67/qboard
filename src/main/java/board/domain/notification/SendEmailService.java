package board.domain.notification;

import java.util.concurrent.CompletionStage;

public interface SendEmailService
{
    CompletionStage<Notification> sendEmail(Notification notification);
}
