package board.port.required;

public interface CacheKeys
{
    String FOLLOWER = "follower";
    String FOLLOWEE = "followee";
    String USER = "user";
    String NOTE = "note";
}
