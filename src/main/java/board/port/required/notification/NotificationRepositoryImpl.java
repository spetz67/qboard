package board.port.required.notification;

import board.domain.notification.Notification;
import board.domain.notification.NotificationRepository;
import board.port.required.jooq.Tables;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

@Singleton
@RequiredArgsConstructor
class NotificationRepositoryImpl extends NotificationRepository
{
    private final DSLContext jooq;
    private final EntityTranslator entityTranslator;

    @Transactional
    @Override
    protected List<Notification> getAllAndLock()
    {
        return getAndLock();
    }

    @Transactional
    @Override
    protected List<Notification> getByNotLockedAndLock()
    {
        return getAndLock(Tables.NOTIFICATION.LOCKED.notEqual(true));
    }

    @Transactional
    @Override
    protected void add(List<Notification> notifications)
    {
        var records = notifications.stream()
                .map(entityTranslator::toNewRecord)
                .toList();
        jooq.batchInsert(records).execute();
    }

    @Transactional
    @Override
    protected void deleteById(Integer id)
    {
        jooq.deleteFrom(Tables.NOTIFICATION)
                .where(Tables.NOTIFICATION.ID.equal(id))
                .execute();
    }

    private List<Notification> getAndLock(Condition... condition)
    {
        List<Notification> notifications = jooq.selectFrom(Tables.NOTIFICATION)
                .where(condition)
                .orderBy(Tables.NOTIFICATION.ID)
                .forUpdate()
                .fetch(entityTranslator::toNotification);

        jooq.update(Tables.NOTIFICATION)
                .set(Tables.NOTIFICATION.LOCKED, true)
                .where(condition)
                .execute();

        return notifications;
    }
}
