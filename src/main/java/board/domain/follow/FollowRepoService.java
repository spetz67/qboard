package board.domain.follow;

import lombok.AllArgsConstructor;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@AllArgsConstructor
public class FollowRepoService
{
    private final FollowRelationRepository repository;

    public FollowRelation get(UUID follower, UUID followee)
    {
        return repository.findById(new FollowRelation.Id(follower, followee))
                .orElseThrow(() -> new FollowRelationNotFoundException(follower, followee));
    }

    public void add(FollowRelation followRelation)
    {
        repository.add(followRelation);
    }

    public void update(FollowRelation followRelation)
    {
        repository.update(followRelation);
    }

    public void delete(UUID follower, UUID followee)
    {
        repository.deleteById(new FollowRelation.Id(follower, followee));
    }
}
