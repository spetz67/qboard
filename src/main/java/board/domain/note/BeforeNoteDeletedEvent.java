package board.domain.note;

public record BeforeNoteDeletedEvent(Note note)
{
}
