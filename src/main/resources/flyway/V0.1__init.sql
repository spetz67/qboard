-- Users
create table "user"
(
    "id"                  uuid                     not null,
    "revision_number"     int                      not null,
    "created_instant"     timestamp with time zone not null,
    "updated_instant"     timestamp with time zone not null,
    "name"                text                     null,
    "total_word_count"    int                      not null,
    "phone_contacts_json" bytea                    null,
    primary key ("id")
);


-- Notes
create table "sequence"
(
    "id"     uuid not null,
    "number" int  not null,
    primary key ("id")
);

create table "note"
(
    "id"      varchar(255)             not null,
    "user_id" uuid                     not null,
    "number"  int                      not null,
    "instant" timestamp with time zone not null,
    "text"    text                     null,
    primary key ("id"),
    foreign key ("user_id") references "user" ("id")
);

create index "number" on "note" ("number");
create index "user_id" on "note" ("user_id");


-- Follow relations
create table "follow_relation"
(
    "id"                                varchar(73)              not null,
    "created_instant"                   timestamp with time zone not null,
    "follower_user_id"                  uuid                     not null,
    "follower_user_name"                text                     not null,
    "followee_user_id"                  uuid                     not null,
    "followee_user_name"                text                     not null,
    "followee_first_unread_note_number" int                      null,
    primary key ("id"),
    foreign key ("follower_user_id") references "user" ("id"),
    foreign key ("followee_user_id") references "user" ("id")
);

create index "follower_user_id" on "follow_relation" ("follower_user_id");
create index "followee_user_id" on "follow_relation" ("followee_user_id");
