package board.domain.common;

import java.util.UUID;

public record Sequence(UUID id, int number)
{
}
