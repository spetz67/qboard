package board.port.provided;

import board.application.Page;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.List;

@Value  //todo change to record when generic records are supported with openapi
public class PageResource<T>
{
    @Schema(description = "The page size, maximum number of elements per page")
    private final int size;

    @Schema(description = "The total number of result elements")
    private final long totalElements;

    @Schema(description = "The total number of pages")
    private final int totalPages;

    @Schema(description = "The number of this page, starting at 0")
    private final int number;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private final List<T> content;

    public static <T> PageResource<T> of(Page<T> page)
    {
        return new PageResource<T>(page.size(), page.totalSize(), page.totalPages(), page.pageNumber(), page.content());
    }
}
