package board.domain.common;

import lombok.Getter;

import java.util.Map;

@Getter
public class TemplatedException extends RuntimeException
{
    private final String code;
    private final Map<String, Object> variables;

    protected TemplatedException(String code, Map<String, Object> variables)
    {
        this(code, null, variables);
    }

    protected TemplatedException(String code, Throwable cause, Map<String, Object> variables)
    {
        super(code+"; "+variables, cause);

        this.code = code;
        this.variables = variables;
    }
}
