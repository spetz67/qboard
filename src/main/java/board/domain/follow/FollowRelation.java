package board.domain.follow;

import board.domain.note.Note;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class FollowRelation
{
    @Getter
    private final Instant createdInstant;

    @Getter
    @Valid
    private Follower follower;

    @Getter
    @Valid
    private Followee followee;

    public record Id(@NotNull UUID followerUserId, @NotNull UUID followeeUserId) {}

    @JsonIgnore
    public FollowRelation.Id getId()
    {
        return new Id(follower.userId(), followee.userId());
    }

    public void resetFolloweeFirstUnreadNoteNumber()
    {
        if (followee.firstUnreadNoteNumber()!=null)
            followee = followee.withFirstUnreadNoteNumber(null);
    }

    void newNoteAvailableFromFollowee(Note note)
    {
        if (followee.firstUnreadNoteNumber()==null || followee.firstUnreadNoteNumber() > note.number())
            followee = followee.withFirstUnreadNoteNumber(note.number());
    }

    void setFollowerName(String followerUserName)
    {
        follower = follower.withUserName(followerUserName);
    }

    void setFolloweeName(String followeeUserName)
    {
        followee = followee.withUserName(followeeUserName);
    }
}
