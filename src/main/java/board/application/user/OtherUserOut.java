package board.application.user;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public record OtherUserOut(

        @NotNull
        UUID id,

        @NotEmpty
        String name,

        boolean canFollow
)
{
}
