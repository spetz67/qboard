package board.application.follow;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

public record FolloweeOut(

        @NotNull
        UUID userId,

        @NotNull
        Instant createdInstant,

        @NotEmpty
        String userName,

        Integer firstUnreadNoteNumber,

        boolean newNoteAvailable
)
{
}
