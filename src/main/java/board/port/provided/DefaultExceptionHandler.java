package board.port.provided;

import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.*;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Slf4j
public class DefaultExceptionHandler implements ExceptionMapper<Exception>
{
    private final UriInfo uriInfo;
    private final Request request;

    @Value
    private static class JsonError
    {
        private String message;
        private String path;
    }

    public DefaultExceptionHandler(@Context UriInfo uriInfo, @Context Request request)
    {
        this.uriInfo = uriInfo;
        this.request = request;
    }

    @Override
    public Response toResponse(Exception e)
    {
        String message = e instanceof WebApplicationException
                ? e.getMessage()
                : e.getClass().getName()+": "+e.getMessage();

        JsonError error = new JsonError(message, uriInfo.getPath());

        Response.Status httpStatus = e instanceof ClientErrorException
                ? Response.Status.BAD_REQUEST
                : Response.Status.INTERNAL_SERVER_ERROR;

        log.error("Error response {} ({}) on {} {}; {}",
                httpStatus.getStatusCode(), httpStatus.getReasonPhrase(), request.getMethod(), uriInfo.getRequestUri(), message);

        return Response
                .status(httpStatus)
                .type(MediaType.APPLICATION_JSON)
                .entity(error)
                .build();
    }
}
