package board.domain.note;

import board.domain.common.SequenceRepository;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteFactory
{
    private final SequenceRepository sequenceRepository;

    @Valid
    public Note createNote(@NotNull UUID userId, @NotEmpty String text, List<Media> media)
    {
        int noteNumber = sequenceRepository.getNext(userId);
        return new Note(userId, noteNumber, Instant.now(), text, media);
    }
}
