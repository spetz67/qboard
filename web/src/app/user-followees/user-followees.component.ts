import {Component} from '@angular/core';
import {FollowingService, NotesService} from "@board-api/services";
import {FolloweeOut} from "@board-api/models";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-user-followees',
    templateUrl: './user-followees.component.html',
    styleUrls: ['./user-followees.component.css']
})
export class UserFolloweesComponent {

    followees: FolloweeOut[] = [];
    notes: Map<string, object> = new Map();

    private userId: string;

    constructor(private followingService: FollowingService,
                private notesService: NotesService,
                private route: ActivatedRoute) {

        this.userId = this.route.snapshot.data.userId
        this.followees = this.route.snapshot.data.followees;
    }

    loadNotes(userId: string, fromNumber: number) {
        this.notesService
                .getNotes({userId: userId, fromNumber: fromNumber, sort: ['number:desc']})
                .subscribe(page => this.notes.set(userId, page.content))
    }

    closeNotes(userId: string, index: number) {
        this.followingService
                .resetNewNoteAvailable({userId: this.userId, followeeUserId: userId, body: {newNoteAvailable: false}})
                .subscribe(() => this.reloadFollowee(userId, index))
    }

    isNotesExpanded(userId: string) {
        return this.notes.has(userId)
    }

    unfollow(followee: FolloweeOut) {
        this.followingService
                .unfollow({userId: this.userId, followeeUserId: followee.userId})
                .subscribe(() => this.followees = this.followees.filter(value => value!==followee))
    }

    private reloadFollowee(userId: string, index: number) {
        this.notes.delete(userId)
        this.followingService
                .getFolloweeByUserId({userId: this.userId, followeeUserId: userId})
                .subscribe(followee => this.followees[index] = followee)
    }
}
