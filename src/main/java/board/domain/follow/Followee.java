package board.domain.follow;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import java.util.UUID;

public record Followee(

        @NotNull
        UUID userId,

        @NotNull
        String userName,

        @Nullable
        Integer firstUnreadNoteNumber
)
{
    public Followee withUserName(@NotNull String userName)
    {
        return new Followee(this.userId, userName, this.firstUnreadNoteNumber);
    }

    public Followee withFirstUnreadNoteNumber(Integer firstUnreadNoteNumber)
    {
        return new Followee(this.userId, this.userName, firstUnreadNoteNumber);
    }
}