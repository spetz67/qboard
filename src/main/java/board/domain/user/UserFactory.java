package board.domain.user;

import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserFactory
{
    @Valid
    public User createUser(@NotEmpty String name)
    {
        return new User(UUID.randomUUID(), 1, Instant.now(), null, name,
                null, 0, List.of(), List.of());
    }
}
