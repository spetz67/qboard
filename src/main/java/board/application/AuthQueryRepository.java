package board.application;

import java.util.Optional;
import java.util.UUID;

public interface AuthQueryRepository
{
    Optional<UUID> findUserIdByEmail(String emailAddress);
}
