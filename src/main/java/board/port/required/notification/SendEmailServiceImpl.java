package board.port.required.notification;

import board.domain.notification.Notification;
import board.domain.notification.NotificationConfiguration;
import board.domain.notification.SendEmailService;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.reactive.ReactiveMailer;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
@RequiredArgsConstructor
class SendEmailServiceImpl implements SendEmailService
{
    private final NotificationConfiguration notificationConfiguration;
    private final ReactiveMailer mailer;

    @Override
    public CompletionStage<Notification> sendEmail(Notification notification)
    {
        if (!notificationConfiguration.isEnabled())
            return CompletableFuture.completedFuture(notification);

        Mail mail = new Mail()
                .addTo(notification.recipient())
                .setSubject(notification.subject())
                .setHtml(notification.content());

        return mailer.send(mail)
                .replaceWith(notification)
                .convert().toCompletionStage();
    }
}
