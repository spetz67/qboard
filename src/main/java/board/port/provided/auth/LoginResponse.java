package board.port.provided.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public record LoginResponse(

        String username,

        Collection<String> roles,

        @JsonProperty("expires_in")
        long expiresIn
)
{
    @JsonProperty("token_type")
    public String tokenType()
    {
        return "Bearer";
    }
}
