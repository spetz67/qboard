package board.domain.note;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

public record Note(

        @NotNull
        UUID userId,

        @Min(1)
        Integer number,

        @NotNull
        Instant instant,

        @NotEmpty
        String text,

        List<Media> media
)
{
    public record Id(@NotNull UUID userId, @Min(1) int number)
    {
    }

    @JsonIgnore
    public Id id()
    {
        return new Id(userId, number);
    }

    public int computeWordCount()
    {
        return text.split(" +").length;
    }
}
