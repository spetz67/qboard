package board.port.provided.follow;

import board.application.follow.FollowQueryRepository;
import board.application.follow.FollowerOut;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/api/users/{userId}/followers")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Following")
public class UserFollowersController
{
    private final FollowQueryRepository queries;

    @GET
    public PageResource<FollowerOut> getFollowers(@PathParam("userId") @NotNull UUID userId,
            @BeanParam PageableParameter pageableParameter)
    {
        return PageResource.of(queries.queryFollowersByUserId(userId, pageableParameter.toPageable()));
    }

    @GET
    @Path("/{followerUserId}")
    public FollowerOut getFollowerByUserId(@PathParam("userId") @NotNull UUID userId,
            @PathParam("followerUserId") UUID followerUserId)
    {
        return queries.queryFollowerByUserAndFollowerUserId(userId, followerUserId);
    }
}