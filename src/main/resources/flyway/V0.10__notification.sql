-- Queued notifications
create table "notification"
(
    "id"        serial,
    "locked"    boolean         not null,
    "recipient" varchar(255)    not null,
    "subject"   text            not null,
    "content"   text            not null,
    primary key ("id")
);
