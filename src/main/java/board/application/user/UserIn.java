package board.application.user;

import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import board.domain.user.User;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import java.util.List;

public record UserIn(

        @Schema(description = "The user name")
        @NotBlank
        String name,

        @Schema(description = "Email address for notifications")
        String notificationEmailAddress,

        List<PhoneContact> phoneContacts,

        List<EmailContact> emailContacts
)
{
    public void copyTo(User user)
    {
        user.setName(name);
        user.setNotificationEmailAddress(notificationEmailAddress);

        user.setPhoneContacts(phoneContacts!=null
                ? phoneContacts
                : List.of());

        user.setEmailContacts(emailContacts!=null
                ? emailContacts
                : List.of());
    }
}
