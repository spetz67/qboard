package board.port.provided.auth;

import io.smallrye.jwt.auth.principal.DefaultJWTParser;
import io.smallrye.jwt.auth.principal.JWTAuthContextInfo;
import io.smallrye.jwt.auth.principal.JWTParser;
import io.smallrye.jwt.auth.principal.ParseException;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperties;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.Set;

@ApplicationScoped
@Slf4j
public class IdTokenVerifier
{
    @ConfigProperties
    SecurityConfig securityConfig;

    private JWTParser jwtParser;

    @PostConstruct
    void init()
    {
        var loginJwtConfig = securityConfig.getLoginJwtConfig();
        var jwtAuthContextInfo = new JWTAuthContextInfo(loginJwtConfig.publicKeyLocation(),
                loginJwtConfig.issuedBy());
        jwtAuthContextInfo.setExpectedAudience(Set.of(loginJwtConfig.expectedAudience()));
        jwtAuthContextInfo.setJwksRefreshInterval(30);

        jwtParser = new DefaultJWTParser(jwtAuthContextInfo);
    }

    public JsonWebToken parseAndVerify(String token)
    {
        try
        {
            return jwtParser.parse(token);
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("Verifying id token failed", e);
        }
    }
}
