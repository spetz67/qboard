package board.domain.note;

import javax.validation.constraints.Min;

public interface NoteConfiguration
{
    @Min(0)
    Integer getMaxNotesPerUser();

    default boolean exceedsMaxNotes(int count)
    {
        Integer maxNotesPerUser = getMaxNotesPerUser();
        return maxNotesPerUser!=null && count > maxNotesPerUser;
    }
}
