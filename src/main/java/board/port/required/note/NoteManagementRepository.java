package board.port.required.note;

import board.domain.note.Note;
import board.port.required.jooq.tables.records.NoteRecord;
import board.port.required.management.AbstractManagementRepository;

import javax.inject.Singleton;

@Singleton
class NoteManagementRepository extends AbstractManagementRepository<Note, NoteRecord>
{
}
