package board.port.provided.follow;

import board.application.follow.FollowCommands;
import board.application.follow.FollowQueryRepository;
import board.application.follow.FolloweeOut;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.UUID;

@Path("/api/users/{userId}/followees")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Following")
public class UserFolloweesController
{
    private final FollowQueryRepository queries;
    private final FollowCommands commands;

    @GET
    public PageResource<FolloweeOut> getFollowees(@PathParam("userId") @NotNull UUID userId,
            @BeanParam PageableParameter pageableParameter)
    {
        return PageResource.of(queries.queryFolloweesByUserId(userId, pageableParameter.toPageable()));
    }

    @GET
    @Path("/{followeeUserId}")
    public FolloweeOut getFolloweeByUserId(@PathParam("userId") @NotNull UUID userId,
            @PathParam("followeeUserId") @NotNull UUID followeeUserId)
    {
        return queries.queryFolloweeByUserAndFolloweeUserId(userId, followeeUserId);
    }

    @POST
    @Path("/{followeeUserId}")
    @APIResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "URI of the created resource",
                    schema = @Schema(type = SchemaType.STRING, format = "uri")))
    public Response follow(@PathParam("userId") @NotNull UUID userId,
            @PathParam("followeeUserId") @NotNull UUID followeeUserId,
            @Context UriInfo uriInfo)
    {
        commands.follow(userId, followeeUserId);
        URI location = uriInfo.getAbsolutePath();
        return Response.created(location).build();
    }

    @PATCH
    @Path("/{followeeUserId}")
    public void resetNewNoteAvailable(@PathParam("userId") @NotNull UUID userId,
            @PathParam("followeeUserId") @NotNull UUID followeeUserId,
            ResetNewNoteAvailablePatchBody body)
    {
        if (Boolean.FALSE == body.getNewNoteAvailable())
            commands.resetFolloweeFirstUnreadNoteNumber(userId, followeeUserId);
    }

    @DELETE
    @Path("/{followeeUserId}")
    public void unfollow(@PathParam("userId") @NotNull UUID userId,
            @PathParam("followeeUserId") @NotNull UUID followeeUserId)
    {
        commands.unfollow(userId, followeeUserId);
    }
}