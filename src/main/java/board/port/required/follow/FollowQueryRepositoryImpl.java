package board.port.required.follow;

import board.application.follow.FollowQueryRepository;
import board.application.Page;
import board.application.Pageable;
import board.domain.follow.FollowRelationNotFoundException;
import board.application.follow.FolloweeOut;
import board.application.follow.FollowerOut;
import board.port.required.CacheKeys;
import board.port.required.Paginator;
import io.quarkus.cache.CacheResult;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.SelectConditionStep;
import org.jooq.SelectFieldOrAsterisk;
import org.jooq.impl.DSL;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

import static board.port.required.jooq.Tables.FOLLOW_RELATION;

@RequiredArgsConstructor
@Singleton
@Transactional
class FollowQueryRepositoryImpl implements FollowQueryRepository
{
    private static final SelectFieldOrAsterisk[] FOLLOWER_FIELDS = {
            FOLLOW_RELATION.FOLLOWER_USER_ID,
            FOLLOW_RELATION.CREATED_INSTANT,
            FOLLOW_RELATION.FOLLOWER_USER_NAME
    };

    private static final SelectFieldOrAsterisk[] FOLLOWEE_FIELDS = {
            FOLLOW_RELATION.FOLLOWEE_USER_ID,
            FOLLOW_RELATION.CREATED_INSTANT,
            FOLLOW_RELATION.FOLLOWEE_USER_NAME,
            FOLLOW_RELATION.FOLLOWEE_FIRST_UNREAD_NOTE_NUMBER,
            DSL.field(FOLLOW_RELATION.FOLLOWEE_FIRST_UNREAD_NOTE_NUMBER.isNotNull())
    };

    private final DSLContext jooq;
    private final Paginator paginator;

    @Override
    @CacheResult(cacheName = CacheKeys.FOLLOWER)
    public Page<FollowerOut> queryFollowersByUserId(UUID userId, Pageable pageable)
    {
        SelectConditionStep<Record> query = jooq
                .select(FOLLOWER_FIELDS)
                .from(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(userId));
        return paginator.queryPage(query, pageable)
                .map(record -> record.into(FollowerOut.class));
    }

    @Override
    @CacheResult(cacheName = CacheKeys.FOLLOWER)
    public FollowerOut queryFollowerByUserAndFollowerUserId(UUID userId, UUID followerUserId)
    {
        return jooq.select(FOLLOWER_FIELDS)
                .from(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(followerUserId), FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(userId))
                .fetchOptionalInto(FollowerOut.class)
                .orElseThrow(() -> new FollowRelationNotFoundException(followerUserId, userId));
    }

    @Override
    @CacheResult(cacheName = CacheKeys.FOLLOWEE)
    public Page<FolloweeOut> queryFolloweesByUserId(UUID userId, Pageable pageable)
    {
        SelectConditionStep<Record> query = jooq
                .select(FOLLOWEE_FIELDS)
                .from(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(userId));
        return paginator.queryPage(query, pageable)
                .map(record -> record.into(FolloweeOut.class));
    }

    @Override
    @CacheResult(cacheName = CacheKeys.FOLLOWEE)
    public FolloweeOut queryFolloweeByUserAndFolloweeUserId(UUID userId, UUID followeeUserId)
    {
        return jooq.select(FOLLOWEE_FIELDS)
                .from(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(userId), FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followeeUserId))
                .fetchOptionalInto(FolloweeOut.class)
                .orElseThrow(() -> new FollowRelationNotFoundException(userId, followeeUserId));
    }
}