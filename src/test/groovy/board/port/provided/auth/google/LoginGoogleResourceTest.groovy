package board.port.provided.auth.google

import board.application.user.UserIn
import board.domain.user.EmailContact
import board.port.provided.AbstractResourceTest
import board.port.provided.auth.AccessTokenGenerator
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.jwt.algorithm.SignatureAlgorithm
import io.smallrye.jwt.auth.principal.DefaultJWTParser
import io.smallrye.jwt.auth.principal.JWTAuthContextInfo
import io.smallrye.jwt.build.Jwt
import org.jose4j.jwk.RsaJwkGenerator
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.inject.Inject
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration

@QuarkusTest
@CompileStatic
class LoginGoogleResourceTest extends AbstractResourceTest
{
    @Inject
    JWTAuthContextInfo authContextInfo

    @Inject
    AccessTokenGenerator accessTokenGenerator

    @Test
    void loginGoogle() {

        def user1 = storeUser(new UserIn("test-user", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.OK.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
              {
                "username": "${user1.id()}",
                "roles": [ "ROLE_USER" ],
                "token_type": "Bearer",
                "expires_in": 3600
              }""", response.body(), JSONCompareMode.LENIENT)


        def accessTokenCookie = getCookie(response, "access-token")
        assert accessTokenCookie.path == '/'
        assert accessTokenCookie.maxAge == -1
        assert accessTokenCookie.httpOnly
        def jwt = new DefaultJWTParser(authContextInfo).parse(accessTokenCookie.value)
        assert jwt.name == user1.id().toString()
        assert jwt.groups == Set.of('ROLE_USER')
    }

    @Test
    void loginGoogle_withExpiredAccessToken()
    {
        def expiredToken = accessTokenGenerator.generate("test-user@test-host", Set.of("ROLE_USER"), Duration.ofSeconds(-60))
        restClient.setAuthToken(expiredToken)

        def user1 = storeUser(new UserIn("test-user", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.OK.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
              {
                "username": "${user1.id()}",
                "roles": [ "ROLE_USER" ],
                "token_type": "Bearer",
                "expires_in": 3600
              }""", response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void loginGoogle_admin()
    {
        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("board.spetz67@gmail.com")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.OK.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
              {
                "username": "board.spetz67@gmail.com",
                "roles": [ "ROLE_USER", "ROLE_ADMIN" ],
                "token_type": "Bearer",
                "expires_in": 3600
              }""", response.body(), JSONCompareMode.LENIENT)

        def accessToken = getCookie(response, "access-token").value
        def jwt = new DefaultJWTParser(authContextInfo).parse(accessToken)
        assert jwt.groups == Set.of('ROLE_USER', 'ROLE_ADMIN')
    }

    @Test
    void loginGoogle_unknown()
    {
        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Login denied: Invalid credentials",
          "path": "/login-google"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void loginGoogle_wrongIssuer()
    {
        userApp.registerUser(new UserIn("test-user", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host", false, "fake.com")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Login denied",
          "path": "/login-google"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void loginGoogle_wrongAudience()
    {
        userApp.registerUser(new UserIn("test-user", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host", false,
                        "accounts.google.com", "498749247398-8732fgo87649ezsdis31oiqdh.apps.googleusercontent.com")))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Login denied",
          "path": "/login-google"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void loginGoogle_wrongSignature()
    {
        userApp.registerUser(new UserIn("test-user", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "test-user@test-host"))))

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/login-google"))
                .POST(HttpRequest.BodyPublishers.ofString(googleIdToken("test-user@test-host", true)))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_PLAIN)
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Login denied",
          "path": "/login-google"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    /**
     * Create a login token like the Google id token, but with our own signature
     */
    private String googleIdToken(String email,
                                 boolean randomSignature = false,
                                 String issuer = "accounts.google.com",
                                 String audience = "506924129809-2hnf3rn0l6hbsnnumqp4v1btunmglher.apps.googleusercontent.com")
    {
//        def testJWK = RsaJwkGenerator.generateJwk(2048)
//        println(testJWK.toJson(JsonWebKey.OutputControlLevel.INCLUDE_PRIVATE))

        def builder = Jwt.audience(audience)
                .issuer(issuer)
                .expiresIn(Duration.ofMinutes(1))
                .subject(UUID.randomUUID().toString())
                .claim("email", email)
                .jws()
                .algorithm(SignatureAlgorithm.RS256)
        def jwt = randomSignature
                ? builder.sign(RsaJwkGenerator.generateJwk(2048).rsaPrivateKey)
                : builder.sign("login-jwt-secret.jwk")
        return jwt
    }

    private HttpCookie getCookie(HttpResponse<String> response, String name)
    {
        for (String header : response.headers().allValues(HttpHeaders.SET_COOKIE))
        {
            for (HttpCookie cookie : HttpCookie.parse(header))
                if (cookie.name == name)
                    return cookie
        }

        return null
    }
}
