package board.domain.note;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotNull;

public record Media(

        @NotNull
        String mediaType,

        @NotNull
        @Schema(format = "byte")    // document base64 format in json
        byte[] content
)
{
}
