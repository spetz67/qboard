package board.port.required.common;

import board.domain.common.Sequence;
import board.port.required.jooq.tables.records.SequenceRecord;
import org.jooq.RecordMapper;
import org.jooq.RecordUnmapper;

import javax.inject.Singleton;

@Singleton
class SequenceTranslator implements RecordMapper<SequenceRecord, Sequence>, RecordUnmapper<Sequence, SequenceRecord>
{
    @Override
    public Sequence map(SequenceRecord record)
    {
        return new Sequence(record.getId(), record.getNumber());
    }

    @Override
    public SequenceRecord unmap(Sequence sequence)
    {
        return new SequenceRecord(sequence.id(), sequence.number());
    }
}
