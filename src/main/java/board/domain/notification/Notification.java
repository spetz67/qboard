package board.domain.notification;

public record Notification(Integer id, boolean locked, String recipient, String subject, String content)
{
}
