package board.domain.note;

import board.domain.common.SequenceRepository;
import board.domain.user.BeforeUserDeletedEvent;
import board.domain.user.UserAddedEvent;
import lombok.RequiredArgsConstructor;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;

@Singleton
@RequiredArgsConstructor
class UserEventHandler
{
    private final SequenceRepository sequenceRepository;
    private final NoteRepository noteRepository;

    void addSequence(@Observes UserAddedEvent event)
    {
        sequenceRepository.addSequence(event.user().getId());
    }

    void deleteSequence(@Observes BeforeUserDeletedEvent event)
    {
        sequenceRepository.deleteById(event.userId());
    }

    void deleteUserNotes(@Observes BeforeUserDeletedEvent event)
    {
        noteRepository.deleteAllByUserId(event.userId());
    }
}
