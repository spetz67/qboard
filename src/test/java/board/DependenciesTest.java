package board;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition;
import com.tngtech.archunit.library.Architectures;

@AnalyzeClasses(importOptions = { ImportOption.DoNotIncludeTests.class, ImportOption.DoNotIncludeArchives.class })
class DependenciesTest
{
    @ArchTest
    static ArchRule ddd = Architectures.onionArchitecture()
            .domainModels("board.domain..")
            .domainServices("board.domain..")
            .applicationServices("board.application..")
            .adapter("provided", "board.port.provided..")
            .adapter("required", "board.port.required..")
            ;

    @ArchTest
    static ArchRule application = ArchRuleDefinition.classes().that()
            .resideInAPackage("board.application..")
            .should().onlyAccessClassesThat()
            .resideInAnyPackage("java..", "javax..", "org.slf4j", "org.eclipse.microprofile..",
                    "com.fasterxml.jackson.databind..", "board.domain..", "board.application..")
            ;

    @ArchTest
    static ArchRule domain = ArchRuleDefinition.classes().that()
            .resideInAPackage("board.domain..")
            .should().onlyAccessClassesThat()
            .resideInAnyPackage("java..", "javax..", "org.slf4j", "org.eclipse.microprofile..",
                    "board.domain..")
            ;
}