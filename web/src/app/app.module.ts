import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ApiModule} from "@board-api/api.module";
import {UsersComponent} from './users/users.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {ErrorHandlerModule} from "./error-handler/error-handler.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {UserDetailsComponent} from './user-details/user-details.component';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatIconModule} from "@angular/material/icon";
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipModule} from "@angular/material/tooltip";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatMenuModule} from "@angular/material/menu";
import {AppRoutingModule} from "./app-routing.module";
import {UserHomeComponent} from './user-home/user-home.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatListModule} from "@angular/material/list";
import {MatCardModule} from "@angular/material/card";
import {UserNotesComponent} from './user-notes/user-notes.component';
import {UserFollowersComponent} from './user-followers/user-followers.component';
import {UserFolloweesComponent} from './user-followees/user-followees.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {UserOthersComponent} from './user-others/user-others.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import {GoogleLoginProvider, SocialLoginModule} from "angularx-social-login";
import {LoginComponent} from './login/login.component';
import {AuthInterceptor} from "./login/auth.interceptor";
import { ManagementComponent } from './management/management.component';


@NgModule({
    declarations: [
        AppComponent,
        UsersComponent,
        UserDetailsComponent,
        UserHomeComponent,
        UserNotesComponent,
        UserFollowersComponent,
        UserFolloweesComponent,
        UserOthersComponent,
        LoginComponent,
        ManagementComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ApiModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        ErrorHandlerModule,
        MatInputModule,
        MatButtonModule,
        MatSelectModule,
        MatIconModule,
        ReactiveFormsModule,
        MatTooltipModule,
        MatToolbarModule,
        MatMenuModule,
        AppRoutingModule,
        MatTabsModule,
        MatListModule,
        MatCardModule,
        FormsModule,
        MatExpansionModule,
        MatCheckboxModule,
        SocialLoginModule,
    ],
    providers: [
        {
            provide: MAT_TOOLTIP_DEFAULT_OPTIONS,
            useValue: {
                showDelay: 1000,
                hideDelay: 0,
                touchendHideDelay: 1000,
            }
        },
        {
            provide: 'SocialAuthServiceConfig',
            useValue: {
                autoLogin: true, //keeps the user signed in
                providers: [
                    {
                        id: GoogleLoginProvider.PROVIDER_ID,
                        provider: new GoogleLoginProvider('506924129809-2hnf3rn0l6hbsnnumqp4v1btunmglher.apps.googleusercontent.com')
                    }
                ]
            }
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
