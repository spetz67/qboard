package board.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

@Singleton
@RequiredArgsConstructor
public class JsonService
{
    private final ObjectMapper objectMapper;

    public <T> List<T> toList(String jsonString, Class<T> type)
    {
        try
        {
            return objectMapper.readValue(jsonString, TypeFactory.defaultInstance().constructCollectionType(List.class, type));
        }
        catch (IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    public <T> T toObject(byte[] jsonBytes, Class<T> type)
    {
        try
        {
            return objectMapper.readValue(jsonBytes, type);
        }
        catch (IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    public <T> T toObject(String jsonString, Class<T> type)
    {
        try
        {
            return objectMapper.readValue(jsonString, type);
        }
        catch (IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    public String toJson(Object value)
    {
        try
        {
            return objectMapper.writeValueAsString(value);
        }
        catch (JsonProcessingException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    public byte[] toJsonBytes(Object value)
    {
        try
        {
            return objectMapper.writeValueAsBytes(value);
        }
        catch (JsonProcessingException e)
        {
            throw new UncheckedIOException(e);
        }
    }
}
