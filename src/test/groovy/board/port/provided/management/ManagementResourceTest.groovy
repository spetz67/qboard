package board.port.provided.management

import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@QuarkusTest
@CompileStatic
class ManagementResourceTest extends AbstractResourceTest
{
    @Test
    void info()
    {
        def response = restClient.get("/management/info")

        assert response.statusCode() == Response.Status.OK.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals('''
              {
                "name": "qboard"
              }''', response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void info_unauthorized()
    {
        restClient.clearAuthToken()
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/management/info")).build())

        assert response.statusCode() == Response.Status.UNAUTHORIZED.statusCode
    }

    @Test
    void info_notPermitted()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/management/info")).build())

        assert response.statusCode() == Response.Status.FORBIDDEN.statusCode
    }

    @Test
    void GET_data()
    {
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/management/data")).build())

        assert response.statusCode() == Response.Status.OK.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == "application/zip"
    }

    @Test
    void PUT_data()
    {
        def uri = baseUri.resolve("/management/data")

        def exportedData = restClient.httpClient
                .send(HttpRequest.newBuilder(uri).build(), HttpResponse.BodyHandlers.ofByteArray())
                .body()

        def response = restClient.exchange(HttpRequest
                .newBuilder(uri)
                .PUT(HttpRequest.BodyPublishers.ofByteArray(exportedData))
                .header(HttpHeaders.CONTENT_TYPE, "application/zip")
                .build())

        assert response.statusCode() == Response.Status.NO_CONTENT.statusCode
    }

    @Test
    void GET_data_notPermitted()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/management/data")).build())

        assert response.statusCode() == Response.Status.FORBIDDEN.statusCode
    }
}
