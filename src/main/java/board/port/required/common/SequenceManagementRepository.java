package board.port.required.common;

import board.domain.common.Sequence;
import board.port.required.jooq.tables.records.SequenceRecord;
import board.port.required.management.AbstractManagementRepository;

import javax.inject.Singleton;

@Singleton
class SequenceManagementRepository extends AbstractManagementRepository<Sequence, SequenceRecord>
{
}
