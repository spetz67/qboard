package board.application.note;

import board.domain.note.Media;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRawValue;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.Instant;

public record NoteOut(

        @Min(1)
        Integer number,

        @NotNull
        Instant instant,

        @NotEmpty
        String text,

        @JsonRawValue
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @Schema(type = SchemaType.ARRAY, implementation = Media.class)
        String media
)
{
}
