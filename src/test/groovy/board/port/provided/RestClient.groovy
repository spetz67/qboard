package board.port.provided

import board.port.provided.auth.AccessTokenGenerator
import groovy.transform.CompileStatic

import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.time.Duration

@Singleton
@CompileStatic
class RestClient
{
    static final String CONTENT_TYPE = HttpHeaders.CONTENT_TYPE
    static final String APPLICATION_JSON = MediaType.APPLICATION_JSON

    URI baseUri = URI.create("http://localhost:8081")

    CookieManager cookieManager = new CookieManager()
    HttpClient httpClient = HttpClient.newBuilder()
            .cookieHandler(cookieManager)
            .build()

    @Inject
    AccessTokenGenerator tokenGenerator
    String authToken

    void setAuthToken(UUID userId, String... roles) {

        def token = tokenGenerator.generate(userId.toString(), Set.of(roles), Duration.ofMinutes(1))
        setAuthToken(token)
    }

    void setAuthToken(String token) {

        authToken = token
        cookieManager.cookieStore.removeAll()
        def cookie = new HttpCookie("access-token", authToken)
        cookie.path = "/"
        cookieManager.cookieStore.add(baseUri, cookie)
    }

    void clearAuthToken() {

        authToken = null
        cookieManager.cookieStore.removeAll()
    }

    HttpResponse<String> exchange(HttpRequest request)
    {
        return httpClient.send(request, HttpResponse.BodyHandlers.ofString())
    }

    HttpResponse<String> get(String uri, String contentType= APPLICATION_JSON)
    {
        def response = httpClient.send(
                HttpRequest
                        .newBuilder(baseUri.resolve(uri))
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .GET()
                        .build(),
                HttpResponse.BodyHandlers.ofString())

        assert response.statusCode() == HttpURLConnection.HTTP_OK
        assert response.headers().firstValue(CONTENT_TYPE).orElse(null) == contentType

        return response
    }

    HttpResponse<String> post(String uri, String request)
    {
        def response = httpClient.send(
                HttpRequest
                        .newBuilder(baseUri.resolve(uri))
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .POST(HttpRequest.BodyPublishers.ofString(request))
                        .build(),
                HttpResponse.BodyHandlers.ofString())

        assert response.statusCode() == HttpURLConnection.HTTP_CREATED
        assert response.headers().firstValue(HttpHeaders.LOCATION).isPresent()

        return response
    }

    HttpResponse<String> put(String uri, String request) {

        def response = httpClient.send(
                HttpRequest
                        .newBuilder(baseUri.resolve(uri))
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .PUT(HttpRequest.BodyPublishers.ofString(request))
                        .build(),
                HttpResponse.BodyHandlers.ofString())

        assert response.statusCode() == HttpURLConnection.HTTP_NO_CONTENT

        return response
    }

    HttpResponse<String> patch(String uri, String request) {

        def response = httpClient.send(
                HttpRequest
                        .newBuilder(baseUri.resolve(uri))
                        .header(CONTENT_TYPE, APPLICATION_JSON)
                        .method('PATCH', HttpRequest.BodyPublishers.ofString(request))
                        .build(),
                HttpResponse.BodyHandlers.ofString())

        assert response.statusCode() == HttpURLConnection.HTTP_NO_CONTENT

        return response
    }

    HttpResponse<String> delete(String uri) {

        def response = httpClient.send(
                HttpRequest
                        .newBuilder(baseUri.resolve(uri)).DELETE()
                        .build(),
                HttpResponse.BodyHandlers.ofString())

        assert response.statusCode() == HttpURLConnection.HTTP_NO_CONTENT
        assert response.headers().firstValue(CONTENT_TYPE).isEmpty()
        assert response.body() == ''

        return response
    }

}
