package board.domain.user;

import java.util.UUID;

public record BeforeUserDeletedEvent(UUID userId)
{
}
