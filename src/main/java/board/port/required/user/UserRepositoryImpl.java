package board.port.required.user;

import board.domain.common.RevisionMismatchException;
import board.domain.user.User;
import board.domain.user.UserRepository;
import board.port.required.CacheKeys;
import board.port.required.jooq.tables.records.UserRecord;
import io.quarkus.cache.CacheInvalidateAll;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.ResultQuery;
import org.jooq.UpdatableRecord;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static board.port.required.jooq.Tables.USER_EMAIL_CONTACT;
import static board.port.required.jooq.Tables.USER_VIEW;
import static board.port.required.jooq.tables.User.USER;

@Singleton
@RequiredArgsConstructor
class UserRepositoryImpl extends UserRepository
{
    private final DSLContext jooq;
    private final UserTranslator userTranslator;

    @Override
    @Transactional
    protected Optional<User> findById(UUID id)
    {
        return jooq.selectFrom(USER_VIEW)
                .where(USER_VIEW.ID.eq(id))
                .fetchOptional(userTranslator::toUser);
    }

    @Override
    @Transactional
    @CacheInvalidateAll(cacheName = CacheKeys.USER)
    protected void add(User user)
    {
        UserRecord record = userTranslator.toRecord(user);
        jooq.executeInsert(record);

        jooq.batchInsert(user.getEmailContacts().stream()
                .map(ec -> userTranslator.toNewRecord(ec, user.getId()))
                .collect(Collectors.toList()))
                .execute();
    }

    @Override
    @Transactional
    @CacheInvalidateAll(cacheName = CacheKeys.USER)
    protected void update(int newRevisionNumber, Instant updatedInstant, User user)
    {
        UserRecord userRecord = userTranslator.toRecord(user);
        userRecord.setUpdatedInstant(updatedInstant);
        userRecord.setRevisionNumber(newRevisionNumber);
        if (jooq.update(USER)
                .set(userRecord)
                .where(USER.ID.eq(user.getId()), USER.REVISION_NUMBER.eq(user.getRevisionNumber()))
                .execute() == 0)
            throw new RevisionMismatchException(user.getRevisionNumber());

        incrementalUpdate(
                jooq.selectFrom(USER_EMAIL_CONTACT).where(USER_EMAIL_CONTACT.USER_ID.equal(user.getId())), //todo ensure same order as returned from user_view
                user.getEmailContacts().stream().map(ec -> userTranslator.toNewRecord(ec, user.getId())),
                USER_EMAIL_CONTACT.ID);
    }

    @Override
    @Transactional
    @CacheInvalidateAll(cacheName = CacheKeys.USER)
    protected boolean deleteById(UUID id)
    {
        boolean deleted = jooq.deleteFrom(USER)
                .where(USER.ID.eq(id))
                .execute() > 0;
        return deleted;
    }

    @Override
    public Optional<String> findNotificationEmailAddress(UUID userId)
    {
        return jooq.select(USER.NOTIFICATION_EMAIL_ADDRESS)
                .from(USER)
                .where(USER.ID.equal(userId))
                .fetchOptional(USER.NOTIFICATION_EMAIL_ADDRESS);
    }

    private <R extends UpdatableRecord<?>, I> void incrementalUpdate(ResultQuery<R> query, Stream<R> newRecords, Field<I> idField)
    {
        var recordIterator = query.iterator();
        var newRecordIterator = newRecords.iterator();

        while (newRecordIterator.hasNext() && recordIterator.hasNext())
        {
            var record = recordIterator.next();
            var newRecord = newRecordIterator.next();
            newRecord.set(idField, record.get(idField));

            if (!newRecord.equals(record))
                jooq.executeUpdate(newRecord);
        }

        recordIterator.forEachRemaining(jooq::executeDelete);
        newRecordIterator.forEachRemaining(jooq::executeInsert);
    }
}
