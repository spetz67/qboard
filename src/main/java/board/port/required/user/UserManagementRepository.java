package board.port.required.user;

import board.domain.management.ManagementRepository;
import board.domain.user.User;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.stream.Stream;

import static board.port.required.jooq.Tables.USER_VIEW;
import static board.port.required.jooq.tables.User.USER;

@Singleton
@RequiredArgsConstructor
class UserManagementRepository implements ManagementRepository<User>
{
    private final DSLContext jooq;
    private final UserTranslator userTranslator;
    private final UserRepositoryImpl userRepository;

    @Override
    public Stream<User> getAll()
    {
        return jooq.selectFrom(USER_VIEW)
                .fetchStream()
                .map(userTranslator::toUser);
    }

    @Override
    @Transactional
    public void deleteAll()
    {
        jooq.deleteFrom(USER).execute();
    }

    @Override
    @Transactional
    public void saveAll(Stream<User> entities)
    {
        entities.forEach(userRepository::add);
    }
}
