package board.domain.notification;

import board.domain.follow.FollowRelation;
import board.domain.follow.FolloweePostedNoteEvent;
import board.domain.user.UserRepoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import java.util.Map;
import java.util.stream.Stream;

@Singleton
@RequiredArgsConstructor
@Slf4j
class FolloweePostedNoteHandler
{
    private final NotificationService notificationService;
    private final NotificationFactory notificationFactory;
    private final UserRepoService userRepoService;

    void notifyFollower(@Observes FolloweePostedNoteEvent event)
    {
        var notifications = event.followRelations().stream()
                .flatMap(this::toNotifications)
                .toList();
        notificationService.publish(notifications);
    }

    private Stream<Notification> toNotifications(FollowRelation followRelation)
    {
        return userRepoService.findNotificationEmailContact(followRelation.getFollower().userId())
                .map(emailAddress -> createEmail(followRelation, emailAddress))
                .stream();
    }

    private Notification createEmail(FollowRelation followRelation, String emailAddress)
    {
        var variables = Map.of(
                "followerName", followRelation.getFollower().userName(),
                "followeeName", followRelation.getFollowee().userName()
        );

        String subject = "New Note on Board";
        String templateResource = "followee-posted-note.template";
        return notificationFactory.create(emailAddress, subject, templateResource, variables);
    }
}
