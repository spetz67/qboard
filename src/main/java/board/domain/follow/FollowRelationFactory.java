package board.domain.follow;

import board.domain.user.User;
import board.domain.user.UserRepoService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class FollowRelationFactory
{
    private final UserRepoService userRepoService;

    @Valid
    public FollowRelation createFollowRelation(@NotNull UUID followerUserId, @NotNull UUID followeeUserId)
    {
        User followerUser = userRepoService.getById(followerUserId);
        User followeeUser = userRepoService.getById(followeeUserId);
        Follower follower = new Follower(followerUserId, followerUser.getName());
        Followee followee = new Followee(followeeUserId, followeeUser.getName(), null);
        return new FollowRelation(now(), follower, followee);
    }

    private Instant now()
    {
        return Instant.now();
    }
}
