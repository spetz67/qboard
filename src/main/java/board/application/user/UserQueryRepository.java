package board.application.user;

import board.application.Page;
import board.application.Pageable;
import board.application.user.OtherUserOut;
import board.domain.user.UserNotFoundException;
import board.application.user.UserOut;

import java.util.UUID;
import java.util.stream.Stream;

public interface UserQueryRepository
{
    Page<UserOut> queryUsers(Pageable pageable);

    UserOut queryUserById(UUID id) throws UserNotFoundException;

    Stream<OtherUserOut> queryOtherUsersByUserId(UUID userId, Pageable pageable);
}
