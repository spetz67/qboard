import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {UsersService} from "@board-api/services/users.service";
import {EmailContact, EmailContactType, PhoneContact, PhoneContactType, UserOut} from "@board-api/models";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

    phoneContactsFA: FormArray = this.fb.array([]);
    emailContactsFA: FormArray = this.fb.array([]);
    form = this.fb.group({
        name: [null, Validators.required],
        notificationEmailAddress: [null, Validators.email],
        phoneContacts: this.phoneContactsFA,
        emailContacts: this.emailContactsFA
    });
    user: UserOut

    phoneContactTypes: PhoneContactType[] = Object.values(PhoneContactType);
    emailContactTypes: EmailContactType[] = Object.values(EmailContactType);

    private userId: string;

    constructor(private fb: FormBuilder,
                private usersService: UsersService,
                private location: Location,
                private route: ActivatedRoute) {

        this.userId = this.route.snapshot.data.userId
        this.user = this.route.snapshot.data.user || { name: '' };
    }

    ngOnInit() {
        this.setUser(this.user)
    }

    goBack() {
        this.location.back()
    }

    saveAndBack(): void {
        this.user.name = this.form.get('name').value
        this.user.notificationEmailAddress = this.form.get('notificationEmailAddress').value
        this.user.phoneContacts = this.phoneContactsFA.value
        this.user.emailContacts = this.emailContactsFA.value

        if (this.isNewUser()) {
            this.usersService
                    .createUser({body: this.user})
                    .subscribe(() => this.goBack())
        } else {
            this.usersService
                    .updateUser({id: this.user.id, body: this.user})
                    .subscribe(() => this.goBack())
        }
    }

    isNewUser(): boolean {
        return !this.userId
    }

    addPhoneContact(phoneContact?: PhoneContact) {
        const formGroup = this.fb.group({
            type: [null, Validators.required],
            number: [null, [Validators.required, Validators.pattern(/^\+[1-9][0-9 \-()./]{7,}$/)]]
        });
        if (phoneContact)
            formGroup.setValue(phoneContact)
        this.phoneContactsFA.push(formGroup)
    }

    deletePhoneContact(index: number) {
        this.phoneContactsFA.removeAt(index)
    }

    addEmailContact(emailContact?: EmailContact) {
        const formGroup = this.fb.group({
            type: [null, Validators.required],
            emailAddress: [null, [Validators.required, Validators.email]]
        });
        if (emailContact)
            formGroup.setValue(emailContact)
        this.emailContactsFA.push(formGroup)
    }

    deleteEmailContact(index: number) {
        this.emailContactsFA.removeAt(index)
    }

    private setUser(userOut: UserOut) {
        this.user = userOut

        if (!this.user.phoneContacts)
            this.user.phoneContacts = []
        if (!this.user.emailContacts)
            this.user.emailContacts = []

        this.form.get('name').setValue(this.user.name)
        this.form.get('notificationEmailAddress').setValue(this.user.notificationEmailAddress)
        this.user.phoneContacts.forEach(phoneContact => this.addPhoneContact(phoneContact))
        this.user.emailContacts.forEach(emailContact => this.addEmailContact(emailContact))
    }
}
