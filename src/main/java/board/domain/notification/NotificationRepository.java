package board.domain.notification;

import java.util.List;

public abstract class NotificationRepository
{
    protected abstract List<Notification> getAllAndLock();

    protected abstract List<Notification> getByNotLockedAndLock();

    protected abstract void add(List<Notification> notification);

    protected abstract void deleteById(Integer id);
}