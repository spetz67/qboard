import {ErrorHandler, Injectable, NgZone} from "@angular/core";
import Swal from 'sweetalert2';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

    constructor(private zone: NgZone) {}

    handleError(error: Error) {

        this.zone.run(() =>
            Swal.fire("Error", error.message, "error"));

        console.error("Error", error);
    }
}