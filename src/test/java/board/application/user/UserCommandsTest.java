package board.application.user;

import board.application.Pageable;
import board.application.follow.FollowCommands;
import board.application.follow.FollowQueryRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
class UserCommandsTest
{
    @Inject
    UserCommands userCommands;

    @Inject
    UserQueryRepository userQueries;

    @Inject
    FollowCommands followCommands;

    @Inject
    FollowQueryRepository followQueries;

    @Test
    void updateUser()
    {
        UUID userId1 = registerUser("user1");
        UUID userId2 = registerUser("user2");
        UUID userId3 = registerUser("user3");
        UUID userId4 = registerUser("user4");
        followCommands.follow(userId1, userId2);
        followCommands.follow(userId1, userId3);
        followCommands.follow(userId4, userId1);

        userCommands.updateUser(userId1, new UserIn("new-name", null, null, null));

        UserOut storedUser = userQueries.queryUserById(userId1);

        assertEquals("new-name", storedUser.name());

        assertEquals(storedUser.name(),
                followQueries.queryFollowerByUserAndFollowerUserId(userId2, userId1).userName());
        assertEquals(storedUser.name(),
                followQueries.queryFollowerByUserAndFollowerUserId(userId3, userId1).userName());
        assertEquals(storedUser.name(),
                followQueries.queryFolloweeByUserAndFolloweeUserId(userId4, userId1).userName());
    }

    @Test
    void deleteUser()
    {
        UUID userId1 = registerUser("user1");
        UUID userId2 = registerUser("user2");
        UUID userId3 = registerUser("user3");
        UUID userId4 = registerUser("user4");
        followCommands.follow(userId1, userId2);
        followCommands.follow(userId1, userId3);
        followCommands.follow(userId4, userId1);

        // check expected follow counts
        assertEquals(1, followQueries.queryFollowersByUserId(userId2, Pageable.unpaged()).totalSize());
        assertEquals(1, followQueries.queryFollowersByUserId(userId3, Pageable.unpaged()).totalSize());
        assertEquals(1, followQueries.queryFolloweesByUserId(userId4, Pageable.unpaged()).totalSize());

        userCommands.deleteUser(userId1);

        // check user1 as followee and follower has been removed
        assertEquals(0, followQueries.queryFollowersByUserId(userId2, Pageable.unpaged()).totalSize());
        assertEquals(0, followQueries.queryFollowersByUserId(userId3, Pageable.unpaged()).totalSize());
        assertEquals(0, followQueries.queryFolloweesByUserId(userId4, Pageable.unpaged()).totalSize());
    }

    private UUID registerUser(String user1)
    {
        return userCommands.registerUser(new UserIn(user1, null, null, null));
    }
}