package board.domain.common;

import java.util.UUID;

public interface SequenceRepository
{
    int getNext(UUID id);

    void addSequence(UUID id);

    void deleteById(UUID id);
}