package board.application.user;

import board.domain.common.RevisionMismatchException;
import board.domain.user.User;
import board.domain.user.UserFactory;
import board.domain.user.UserNotFoundException;
import board.domain.user.UserRepoService;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.faulttolerance.Retry;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserCommands
{
    private final UserRepoService repoService;
    private final UserFactory factory;
    private final UserQueryRepository queryRepository;

    public UUID registerUser(UserIn userIn)
    {
        User user = factory.createUser(userIn.name());
        userIn.copyTo(user);
        repoService.add(user);
        return user.getId();
    }

    @Retry(retryOn = RevisionMismatchException.class)
    public void updateUser(UUID userId, UserIn userIn) throws UserNotFoundException
    {
        User user = repoService.getById(userId);
        userIn.copyTo(user);
        repoService.update(user);
    }

    public void deleteUser(UUID userId)
    {
        repoService.delete(userId);
    }
}