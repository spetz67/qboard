package board.port.provided.user;

import board.application.note.NoteCommands;
import board.application.note.NoteQueryRepository;
import board.application.note.NoteIn;
import board.domain.note.NoteNotFoundException;
import board.application.note.NoteOut;
import board.domain.note.Note;
import board.port.provided.PageResource;
import board.port.provided.PageableParameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.headers.Header;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.util.UUID;

@Path("/api/users/{userId}/notes")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@Slf4j
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Notes")
public class UserNotesController
{
    private final NoteQueryRepository queries;
    private final NoteCommands commands;

    @GET
    public PageResource<NoteOut> getNotes(@PathParam("userId") @NotNull UUID userId,
            @QueryParam("fromNumber") @Nullable Integer fromNumber,
            @BeanParam PageableParameter pageableParameter)
    {
        return PageResource.of(queries.queryNotesByUserId(userId, fromNumber, pageableParameter.toPageable()));
    }

    @GET
    @Path("/{number}")
    public NoteOut getNoteByNumber(@PathParam("userId") @NotNull UUID userId, @PathParam("number") int number)
    {
        return queries.queryNoteByUserIdAndNumber(userId, number);
    }

    @DELETE
    @Path("/{number}")
    public void deleteNoteByNumber(@PathParam("userId") @NotNull UUID userId, @PathParam("number") int number)
    {
        try
        {
            commands.deleteNoteByUserAndNumber(userId, number);
        }
        catch (NoteNotFoundException e)
        {
            // REST DELETE must not fail when resource is not there, so just log this
            log.warn("Note {} not found with user id {}", number, userId);
        }
    }

    @POST
    @APIResponse(responseCode = "201", description = "Resource created",
            headers = @Header(name = "location", description = "URI of the created resource",
                    schema = @Schema(type = SchemaType.STRING, format = "uri")))
    public Response postNote(@PathParam("userId") @NotNull UUID userId,
            @Valid NoteIn noteIn,
            @Context UriInfo uriInfo)
    {
        Note note = commands.postNote(userId, noteIn);
        URI location = uriInfo.getAbsolutePathBuilder()
                .path(String.valueOf(note.number()))
                .build();
        return Response.created(location).build();
    }
}