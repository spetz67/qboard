package board.port.provided.follow;

import lombok.Data;

@Data
public class ResetNewNoteAvailablePatchBody
{
    private Boolean newNoteAvailable;
}
