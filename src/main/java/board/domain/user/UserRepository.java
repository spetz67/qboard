package board.domain.user;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

public abstract class UserRepository
{
    protected abstract Optional<User> findById(UUID id);

    protected abstract void add(@Valid User user);

    protected abstract void update(int newRevisionNumber, Instant updatedInstant, @Valid User user);

    protected abstract boolean deleteById(@NotNull UUID userId);

    protected abstract Optional<String> findNotificationEmailAddress(@NotNull UUID userId);
}