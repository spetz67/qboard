package board.domain.notification;

public interface NotificationConfiguration
{
    String getBoardUrl();
    boolean isEnabled();
}
