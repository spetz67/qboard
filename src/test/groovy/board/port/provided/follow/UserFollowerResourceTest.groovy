package board.port.provided.follow

import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.net.http.HttpRequest

@QuarkusTest
@CompileStatic
class UserFollowerResourceTest extends AbstractResourceTest
{
    @Test
    void getFollowers_2()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        def followee1 = follow(user2, user1.id())   // createdInstant is same as in follower
        def followee2 = follow(user3, user1.id())

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}/followers")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "userId": "${user2.id()}",
              "createdInstant": "${followee1.createdInstant()}",
              "userName": "user2"
            },
            {
              "userId": "${user3.id()}",
              "createdInstant": "${followee2.createdInstant()}",
              "userName": "user3"
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.NON_EXTENSIBLE)
    }

    @Test
    void getFollower()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        storeUser("user3")
        def followee1 = follow(user2, user1.id())   // createdInstant is same as in follower

        def response = restClient.get("/api/users/${user1.id()}/followers/${user2.id()}")

        JSONAssert.assertEquals("""
        {
            "userId": "${user2.id()}",
            "createdInstant": "${followee1.createdInstant()}",
            "userName": "user2"
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getFollower_notFound()
    {
        def user1 = storeUser("user1")
        def followerUuid = UUID.randomUUID()

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/api/users/${user1.id()}/followers/${ followerUuid}"))
                .build())

        assert response.statusCode() == Response.Status.NOT_FOUND.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Follow relation not found for follower=$followerUuid and followee=${user1.id()}",
          "path": "/api/users/${user1.id()}/followers/${followerUuid}"
        }""", (String)response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        follow(user2, user1.id())
        follow(user3, user1.id())

        def response = restClient.get("/api/users/${user1.id()}")

        // verify followerCount
        JSONAssert.assertEquals("""
        {
          "id": "${user1.id()}",
          "name": "user1",
          "totalWordCount": 0,
          "followerCount": 2,
          "phoneContacts": [],
          "emailContacts": []
        }""", response.body(), JSONCompareMode.STRICT)
    }
}
