package board.port.required;

import io.quarkus.cache.CacheInvalidateAll;
import org.jooq.DSLContext;
import org.jooq.Table;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;

import static board.port.required.jooq.Tables.*;

@Singleton
public class TestRepository
{
    @Inject
    DSLContext jooq;

    @CacheInvalidateAll(cacheName = "user")
    @CacheInvalidateAll(cacheName = "note")
    @CacheInvalidateAll(cacheName = "follower")
    @CacheInvalidateAll(cacheName = "followee")
    @Transactional
    public void deleteAll()
    {
        for (Table<?> table: List.of(SEQUENCE, NOTE, FOLLOW_RELATION, USER, USER_EMAIL_CONTACT, NOTIFICATION))
            jooq.deleteFrom(table).execute();
    }
}