package board.port.required.user;

import board.application.Page;
import board.application.Pageable;
import board.application.user.UserQueryRepository;
import board.application.user.OtherUserOut;
import board.domain.user.UserNotFoundException;
import board.application.user.UserOut;
import board.port.required.CacheKeys;
import board.port.required.Paginator;
import io.quarkus.cache.CacheResult;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import static board.port.required.jooq.Tables.*;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.notExists;

@Singleton
@RequiredArgsConstructor
class UserQueryRepositoryImpl implements UserQueryRepository
{
    private final DSLContext jooq;
    private final Paginator paginator;
    private final UserTranslator translator;

    @Transactional
    @Override
    @CacheResult(cacheName = CacheKeys.USER)
    public Page<UserOut> queryUsers(Pageable pageable)
    {
        List<UserOut> users = jooq.selectFrom(USER_VIEW)
                .orderBy(paginator.getSortFields(pageable))
                .offset(pageable.offset())
                .limit(pageable.size()>0 ? pageable.size() : Integer.MAX_VALUE)
                .fetch(translator::toUserOut);
        int totalSize = jooq.fetchCount(USER_VIEW);
        return new Page<>(users, pageable, totalSize);
    }

    @Transactional
    @Override
    @CacheResult(cacheName = CacheKeys.USER)
    public UserOut queryUserById(UUID userId)
    {
        return jooq.selectFrom(USER_VIEW)
                .where(USER_VIEW.ID.equal(userId))
                .fetchOptional(translator::toUserOut)
                .orElseThrow(() -> new UserNotFoundException(userId));
    }

    @Override
    public Stream<OtherUserOut> queryOtherUsersByUserId(UUID userId, Pageable pageable)
    {
        return jooq.select(USER.ID, USER.NAME,
                field(notExists(jooq.select()
                        .from(FOLLOW_RELATION)
                        .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.equal(USER.ID), FOLLOW_RELATION.FOLLOWER_USER_ID.equal(userId))))
                        .as("can_follow"))
                .from(USER)
                .where(USER.ID.notEqual(userId))
                .orderBy(paginator.getSortFields(pageable))
                .offset(pageable.offset())
                .limit(pageable.size()>0 ? pageable.size() : Integer.MAX_VALUE)
                .fetchStreamInto(OtherUserOut.class);
    }
}