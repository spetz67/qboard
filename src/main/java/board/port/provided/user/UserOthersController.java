package board.port.provided.user;

import board.application.user.UserQueryRepository;
import board.application.user.OtherUserOut;
import board.port.provided.PageableParameter;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Path("/api/users/{userId}/others")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Users")
public class UserOthersController
{
    private final UserQueryRepository queries;

    @GET
    @Transactional
    public List<? extends OtherUserOut> getOthers(@PathParam("userId") @NotNull UUID userId,
            @BeanParam PageableParameter pageableParameter)
    {
        return queries.queryOtherUsersByUserId(userId, pageableParameter.toPageable()).collect(Collectors.toList());
    }
}