package board.port.required.note;

import board.domain.note.Note;
import board.domain.note.NoteRepository;
import board.port.required.CacheKeys;
import board.port.required.jooq.Keys;
import io.quarkus.cache.CacheInvalidateAll;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

import static board.port.required.jooq.tables.Note.NOTE;

@RequiredArgsConstructor
@Singleton
class NoteRepositoryImpl extends NoteRepository
{
    private final DSLContext jooq;
    private final NoteTranslator noteTranslator;

    @Override
    @Transactional
    protected int getNoteCountByUserId(UUID userId)
    {
        return jooq.fetchCount(NOTE, NOTE.USER_ID.eq(userId));
    }

    @Override
    protected Optional<Note> findById(Note.Id id)
    {
        return jooq.selectFrom(NOTE)
                .where(primaryKeyEquals(id))
                .fetchOptional(noteTranslator);
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.NOTE)
    protected void add(Note note)
    {
        var record = noteTranslator.unmap(note);
        jooq.executeInsert(record);
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.NOTE)
    protected void deleteById(Note.Id id)
    {
        jooq.deleteFrom(NOTE)
                .where(primaryKeyEquals(id))
                .execute();
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.NOTE)
    protected void deleteAllByUserId(UUID userId)
    {
        jooq.deleteFrom(NOTE)
                .where(NOTE.USER_ID.eq(userId))
                .execute();
    }

    private Condition primaryKeyEquals(Note.Id id)
    {
        return DSL.row(Keys.NOTE_PKEY.getFields()).equal(id.userId(), id.number());
    }
}