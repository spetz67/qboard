package board.port.required.follow;

import board.domain.follow.FollowRelation;
import board.domain.follow.FollowRelationRepository;
import board.port.required.CacheKeys;
import board.port.required.jooq.Keys;
import io.quarkus.cache.CacheInvalidateAll;
import lombok.RequiredArgsConstructor;
import org.jooq.Condition;
import org.jooq.DSLContext;
import org.jooq.impl.DSL;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

import static board.port.required.jooq.tables.FollowRelation.FOLLOW_RELATION;

@RequiredArgsConstructor
@Singleton
class FollowRelationRepositoryImpl extends FollowRelationRepository
{
    private final DSLContext jooq;
    private final FollowTranslator followTranslator;

    @Override
    @Transactional
    protected Optional<FollowRelation> findById(FollowRelation.Id id)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(primaryKeyEquals(id))
                .fetchOptional(followTranslator);
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWER)
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWEE)
    @Transactional
    protected void add(FollowRelation followRelation)
    {
        jooq.executeInsert(followTranslator.unmap(followRelation));
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWER)
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWEE)
    @Transactional
    protected void update(FollowRelation followRelation)
    {
        jooq.update(FOLLOW_RELATION)
                .set(followTranslator.unmap(followRelation))
                .where(primaryKeyEquals(followRelation.getId()))
                .execute();
    }

    @Override
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWER)
    @CacheInvalidateAll(cacheName = CacheKeys.FOLLOWEE)
    @Transactional
    protected void deleteById(FollowRelation.Id id)
    {
        jooq.deleteFrom(FOLLOW_RELATION)
                .where(primaryKeyEquals(id))
                .execute();
    }

    @Override
    @Transactional
    protected Stream<FollowRelation> getByFollower(UUID follower)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWER_USER_ID.eq(follower))
                .fetchStream()
                .map(followTranslator::map);
    }

    @Override
    @Transactional
    protected Stream<FollowRelation> getByFollowee(UUID followee)
    {
        return jooq.selectFrom(FOLLOW_RELATION)
                .where(FOLLOW_RELATION.FOLLOWEE_USER_ID.eq(followee))
                .fetchStream()
                .map(followTranslator::map);
    }

    private Condition primaryKeyEquals(FollowRelation.Id id)
    {
        return DSL.row(Keys.FOLLOW_RELATION_PKEY.getFields()).equal(id.followerUserId(), id.followeeUserId());
    }
}
