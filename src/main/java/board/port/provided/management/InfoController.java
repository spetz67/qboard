package board.port.provided.management;

import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Properties;

@Path("/management/info")
@Produces(MediaType.APPLICATION_JSON)
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Management")
public class InfoController
{
    @GET
    public Properties info() throws IOException
    {
        Properties buildInfo = new Properties();
        buildInfo.load(getClass().getResourceAsStream("/META-INF/build-info.properties"));
        return buildInfo;
    }
}