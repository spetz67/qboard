package board.application.user;

import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRawValue;
import org.eclipse.microprofile.openapi.annotations.enums.SchemaType;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

public record UserOut(

        @NotNull
        UUID id,

        @JsonIgnore
        Instant updatedInstant,

        int totalWordCount,

        int followerCount,

        @Schema(description = "The user name")
        @NotBlank
        String name,

        @Schema(description = "Email address for notifications")
        String notificationEmailAddress,

        @JsonRawValue
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @Schema(type = SchemaType.ARRAY, implementation = PhoneContact.class)
        String phoneContacts,

        @JsonRawValue
        @JsonInclude(JsonInclude.Include.NON_NULL)
        @Schema(type = SchemaType.ARRAY, implementation = EmailContact.class)
        String emailContacts
)
{
}
