package board.domain.note;

import lombok.RequiredArgsConstructor;

import javax.enterprise.event.Event;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteRepoService
{
    private final NoteRepository noteRepository;
    private final Event<Object> eventService;

    @Transactional
    public void add(Note note)
    {
        noteRepository.add(note);
        eventService.fire(new NotePostedEvent(note));
    }

    @Transactional
    public void delete(UUID userId, int noteNumber)
    {
        noteRepository.findById(new Note.Id(userId, noteNumber))
                .ifPresent(note -> {
                    eventService.fire(new BeforeNoteDeletedEvent(note));
                    noteRepository.deleteById(note.id());
                });
    }

    public int getNoteCountByUserId(UUID userId)
    {
        return noteRepository.getNoteCountByUserId(userId);
    }
}
