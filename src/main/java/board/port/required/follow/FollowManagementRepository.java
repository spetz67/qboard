package board.port.required.follow;

import board.domain.follow.FollowRelation;
import board.port.required.jooq.tables.records.FollowRelationRecord;
import board.port.required.management.AbstractManagementRepository;

import javax.inject.Singleton;

@Singleton
class FollowManagementRepository extends AbstractManagementRepository<FollowRelation, FollowRelationRecord>
{
}
