package board.port.required.notification;

import board.domain.notification.NotificationConfiguration;
import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;

@Singleton
@Getter
class NotificationConfigurationImpl implements NotificationConfiguration
{
    @ConfigProperty(name = "board.notification.board-url", defaultValue = "https://board.steffen-petz.de")
    String boardUrl;

    @ConfigProperty(name = "board.notification.enabled", defaultValue = "false")
    boolean enabled;
}
