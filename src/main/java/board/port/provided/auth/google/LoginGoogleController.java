package board.port.provided.auth.google;

import board.application.AuthQueryRepository;
import board.port.provided.auth.AccessTokenGenerator;
import board.port.provided.auth.IdTokenVerifier;
import board.port.provided.auth.LoginResponse;
import board.port.provided.auth.SecurityConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperties;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.time.Duration;
import java.util.Set;
import java.util.UUID;

@Path("/login-google")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
@Slf4j
@Tag(name = "Login")
public class LoginGoogleController
{
    private final IdTokenVerifier verifier;
    private final AuthQueryRepository authQueryRepository;
    private final AccessTokenGenerator accessTokenGenerator;

    @ConfigProperties
    SecurityConfig securityConfig;

    private record UserDetails(String user, Set<String> roles) { }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @APIResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = LoginResponse.class)))
    public Response loginGoogle(String idToken, @Context UriInfo uriInfo)
    {
        try
        {
            UserDetails userDetails = getUserDetails(idToken);
            Duration expiresIn = Duration.ofHours(1);
            String token = accessTokenGenerator.generate(userDetails.user(), userDetails.roles(), expiresIn);
            var response = new LoginResponse(userDetails.user(), userDetails.roles(), expiresIn.getSeconds());
            return Response.ok(response)
                    .cookie(createAccessTokenCookie(uriInfo, token))
                    .build();
        }
        catch (IllegalArgumentException e)
        {
            log.error("Verifying id token failed", e);
            throw new BadRequestException("Login denied");
        }
    }

    private UserDetails getUserDetails(String idToken)
    {
        String emailAddress = getEmailAddress(idToken);

        if (emailAddress.equals(securityConfig.getAdmin()))
            return new UserDetails(emailAddress, Set.of("ROLE_ADMIN", "ROLE_USER"));

        String userId = authQueryRepository
                .findUserIdByEmail(emailAddress)
                .map(UUID::toString)
                .orElseThrow(() -> new CredentialsNotFoundException(emailAddress));
        return new UserDetails(userId, Set.of("ROLE_USER"));
    }

    private String getEmailAddress(String idTokenString)
    {
        JsonWebToken idToken = verifier.parseAndVerify(idTokenString);
        return idToken.getClaim("email");
    }

    private NewCookie createAccessTokenCookie(UriInfo uriInfo, String token)
    {
        boolean secureConnection = uriInfo.getRequestUri().getScheme().equalsIgnoreCase("https");
        return new NewCookie("access-token", token, "/", null, null, -1,
                secureConnection, true);
    }
}