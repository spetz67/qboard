package board.domain.common;

import java.util.Collection;
import java.util.LinkedList;

public class EventQueue
{
    private Collection<Object> events = new LinkedList<>();

    public void add(Object event)
    {
        events.add(event);
    }

    public Collection<Object> takeEvents()
    {
        Collection<Object> returnedEvents = events;
        events = new LinkedList<>();
        return returnedEvents;
    }
}