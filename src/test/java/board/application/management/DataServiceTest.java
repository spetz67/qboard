package board.application.management;

import board.application.JsonService;
import board.domain.management.MetaInfo;
import board.domain.common.Sequence;
import board.domain.follow.FollowRelation;
import board.domain.management.ManagementRepository;
import board.domain.note.Note;
import board.domain.user.User;
import io.quarkus.test.junit.QuarkusTest;
import org.json.JSONException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import javax.inject.Inject;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@QuarkusTest
class DataServiceTest
{
    private static final String USERS_NDJSON = """
        {"id":"a98e3f36-5ca3-41a1-8a32-7a87f3e8c64d","revisionNumber":1,"createdInstant":"2021-12-23T10:16:35.137277Z","updatedInstant":"2021-12-23T10:16:35.138926Z","name":"user1","totalWordCount":0,"phoneContacts":[],"emailContacts":[{"type":"PRIVATE","emailAddress":"user1@board.com"}]}
        {"id":"c579b71a-8217-48ba-b796-3ceb40633e78","revisionNumber":1,"createdInstant":"2021-12-23T10:16:35.217204Z","updatedInstant":"2021-12-23T10:16:35.217426Z","name":"user3","totalWordCount":0,"phoneContacts":[{"type":"PRIVATE","number":"0123"}],"emailContacts":[], "notificationEmailAddress": "user3@board"}
        {"id":"d1f226da-bb75-4768-bca3-733cdfd85b0e","revisionNumber":2,"createdInstant":"2021-12-23T10:16:35.209998Z","updatedInstant":"2021-12-23T10:16:35.315794Z","name":"user2","totalWordCount":1,"phoneContacts":[],"emailContacts":[{"type":"PRIVATE","emailAddress":"user2@board.com"}]}
    """;

    private static final String NOTES_NDJSON = """
        {"userId":"d1f226da-bb75-4768-bca3-733cdfd85b0e","number":1,"instant":"2021-12-23T10:35:19.550166Z","text":"message2"}
        {"userId":"c579b71a-8217-48ba-b796-3ceb40633e78","number":1,"instant":"2021-12-23T10:35:19.585067Z","text":"message3", "media": [{"mediaType": "image/png", "content": "aW1hZ2UK"}]}
        {"userId":"d1f226da-bb75-4768-bca3-733cdfd85b0e","number":2,"instant":"2021-12-23T10:35:19.752505Z","text":"message2.1"}
    """;

    private static final String FOLLOWS_NDJSON = """
        {"createdInstant":"2021-12-23T10:16:35.240067Z","follower":{"userId":"a98e3f36-5ca3-41a1-8a32-7a87f3e8c64d","userName":"user1"},"followee":{"userId":"d1f226da-bb75-4768-bca3-733cdfd85b0e","userName":"user2","firstUnreadNoteNumber":1}}
        {"createdInstant":"2021-12-23T10:16:35.264711Z","follower":{"userId":"d1f226da-bb75-4768-bca3-733cdfd85b0e","userName":"user2"},"followee":{"userId":"c579b71a-8217-48ba-b796-3ceb40633e78","userName":"user3","firstUnreadNoteNumber":1}}
        {"createdInstant":"2021-12-23T10:16:35.256761Z","follower":{"userId":"a98e3f36-5ca3-41a1-8a32-7a87f3e8c64d","userName":"user1"},"followee":{"userId":"c579b71a-8217-48ba-b796-3ceb40633e78","userName":"user3","firstUnreadNoteNumber":1}}
    """;

    private static final String SEQUENCES_NDJSON = """
        {"id":"d1f226da-bb75-4768-bca3-733cdfd85b0e","number":2}
        {"id":"c579b71a-8217-48ba-b796-3ceb40633e78","number":3}
    """;


    @Inject
    ManagementRepository<User> userManagementRepository;

    @Inject
    ManagementRepository<Note> noteManagementRepository;

    @Inject
    ManagementRepository<FollowRelation> followRelationManagementRepository;

    @Inject
    ManagementRepository<Sequence> sequenceManagementRepository;

    @Inject
    DataService dataService;

    @Inject
    JsonService jsonService;

    @BeforeEach
    void setUp()
    {
        sequenceManagementRepository.deleteAll();
        followRelationManagementRepository.deleteAll();
        noteManagementRepository.deleteAll();
        userManagementRepository.deleteAll();
    }

    @Test
    void exportAsZip() throws IOException, JSONException
    {
        userManagementRepository.saveAll(toObjectStream(USERS_NDJSON, User.class));
        noteManagementRepository.saveAll(toObjectStream(NOTES_NDJSON, Note.class));
        followRelationManagementRepository.saveAll(toObjectStream(FOLLOWS_NDJSON, FollowRelation.class));
        sequenceManagementRepository.saveAll(toObjectStream(SEQUENCES_NDJSON, Sequence.class));


        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        dataService.exportAsZip(baos);
        ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(baos.toByteArray()));

        Properties buildInfo = new Properties();
        buildInfo.load(getClass().getResourceAsStream("/META-INF/build-info.properties"));
        String appVersion = buildInfo.getProperty("version");

        zip.getNextEntry();
        String expectedMetaInfoJson = jsonService.toJson(new MetaInfo("board", appVersion, 1, null));
        JSONAssert.assertEquals(expectedMetaInfoJson, new String(zip.readAllBytes()), false);

        compareNdJson(USERS_NDJSON, zip);
        compareNdJson(NOTES_NDJSON, zip);
        compareNdJson(FOLLOWS_NDJSON, zip);
        compareNdJson(SEQUENCES_NDJSON, zip);

        zip.close();
    }

    @Test
    void importFromZip() throws IOException
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(baos);

        zip.putNextEntry(new ZipEntry("meta-info.json"));
        zip.write(jsonService.toJsonBytes(new MetaInfo("board", "0.16", 1, null)));

        zip.putNextEntry(new ZipEntry("users.ndjson"));
        zip.write(USERS_NDJSON.getBytes(StandardCharsets.UTF_8));

        zip.putNextEntry(new ZipEntry("notes.ndjson"));
        zip.write(NOTES_NDJSON.getBytes(StandardCharsets.UTF_8));

        zip.putNextEntry(new ZipEntry("followRelations.ndjson"));
        zip.write(FOLLOWS_NDJSON.getBytes(StandardCharsets.UTF_8));

        zip.putNextEntry(new ZipEntry("sequences.ndjson"));
        zip.write(SEQUENCES_NDJSON.getBytes(StandardCharsets.UTF_8));

        zip.close();


        dataService.importFromZip(new ByteArrayInputStream(baos.toByteArray()));


        Assertions.assertEquals(3, userManagementRepository.getAll().count());
        Assertions.assertEquals(3, noteManagementRepository.getAll().count());
        Assertions.assertEquals(3, followRelationManagementRepository.getAll().count());
        Assertions.assertEquals(2, sequenceManagementRepository.getAll().count());
    }

    private <T> Stream<T> toObjectStream(String ndjson, Class<T> type)
    {
        return new BufferedReader(new StringReader(ndjson))
                .lines()
                .map(line -> jsonService.toObject(line, type));
    }

    private void compareNdJson(String expectedLines, ZipInputStream zip) throws IOException, JSONException
    {
        zip.getNextEntry();
        var lineReader = new BufferedReader(new InputStreamReader(zip, StandardCharsets.UTF_8));
        for (String line: expectedLines.split("\n"))
            JSONAssert.assertEquals(line, lineReader.readLine(), true);
        zip.closeEntry();
    }
}