package board.domain.user;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public record PhoneContact(

        @NotNull
        Type type,

        @NotEmpty
        String number
)
{
    @Schema(name = "PhoneContactType")
    public enum Type { PRIVATE, COMPANY, MOBILE }
}
