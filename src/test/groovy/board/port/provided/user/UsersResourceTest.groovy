package board.port.provided.user

import board.application.Pageable
import board.application.user.UserIn
import board.domain.user.EmailContact
import board.domain.user.PhoneContact
import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.jboss.resteasy.util.DateUtil
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.net.http.HttpRequest
import java.time.temporal.ChronoUnit

@QuarkusTest
@CompileStatic
class UsersResourceTest extends AbstractResourceTest
{
    @Test
    void createUser()
    {
        //language=json
        String request = """
        {
          "name":"test-user",
          "notificationEmailAddress": "notification@test-user.com",
          "phoneContacts": [
            {
              "type": "PRIVATE",
              "number": "1234"
            }
          ],
          "emailContacts": [
            {
              "type": "PRIVATE",
              "emailAddress": "a@b.c"
            },
            {
              "type": "COMPANY",
              "emailAddress": "x@y.z"
            }
          ]
        }"""

        def response = restClient.post("/api/users", request)

        def createdUser = userQueries.queryUsers(Pageable.unpaged()).content().first()
        def userId = createdUser.id()

        assert response.statusCode() == Response.Status.CREATED.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == null
        assert response.headers().firstValue(HttpHeaders.LOCATION).orElse(null) == baseUri.resolve("/api/users/$userId").toString()
        assert response.body().isEmpty()

        assert createdUser.name() == "test-user"
        assert createdUser.notificationEmailAddress() == "notification@test-user.com"
        JSONAssert.assertEquals('''[
            { "type": "PRIVATE", "number": "1234" } 
        ]''', createdUser.phoneContacts(), JSONCompareMode.STRICT)
        JSONAssert.assertEquals('''[
            { "type": "PRIVATE", "emailAddress": "a@b.c" },
            { "type": "COMPANY", "emailAddress": "x@y.z" }
        ]''', createdUser.emailContacts(), JSONCompareMode.STRICT)
    }

    @Test
    void createUser_invalidName()
    {
        //language=json
        String request = """
        {
          "name":""
        }"""

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve("/api/users"))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .POST(HttpRequest.BodyPublishers.ofString(request))
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        // there should be a simple "message" property, but this is what ResteasyViolationExceptionMapper generates
        JSONAssert.assertEquals("""{
          "propertyViolations": [],
          "classViolations": [],
          "parameterViolations": [
            {
              "constraintType": "PARAMETER",
              "path": "createUser.userIn.name",
              "message": "must not be blank",
              "value": ""
            }
          ],
          "returnValueViolations": []
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void getUser()
    {
        def user1 = storeUser(new UserIn("user-1", "notification@test-user.com",
                List.of(new PhoneContact(PhoneContact.Type.PRIVATE, "1234")), null))
        postNote(user1, "message 1")

        sleep(1100) // ensure user last modified time is different within http header time resolution
        postNote(user1, "message 2")

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}")

        def expectedLastModified = userQueries.queryUserById(user1.id()).updatedInstant().truncatedTo(ChronoUnit.SECONDS)
        assert DateUtil.parseDate(response.headers().firstValue(HttpHeaders.LAST_MODIFIED).orElse(null)).toInstant() == expectedLastModified

        JSONAssert.assertEquals("""
        {
          "id": "${user1.id()}",
          "name": "user-1",
          "notificationEmailAddress": "notification@test-user.com",
          "totalWordCount": 4,
          "followerCount": 0,
          "phoneContacts": [
            {
              "type": "PRIVATE",
              "number": "1234"
            }
          ],
          "emailContacts": []
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser_notFound()
    {
        def id = UUID.randomUUID()

        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users/$id")).build())

        assert response.statusCode() == Response.Status.NOT_FOUND.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "User not found by id: $id",
          "path": "/api/users/$id"
        }""", (String)response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUser_malformedId()
    {
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users/xxx")).build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "RESTEASY003870: Unable to extract parameter from http request: javax.ws.rs.PathParam(\\"id\\") value is 'xxx'",
          "path": "/api/users/xxx"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void getUser_invalidSortOrder()
    {
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users?sort=name:xxx")).build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Invalid sort parameter",
          "path": "/api/users"
        }""", (String)response.body(), JSONCompareMode.LENIENT)
    }

    @Test
    void getUser_differentPrincipal()
    {
        def user1 = storeUser("user-1")
        def user2 = storeUser("user-2")

        restClient.setAuthToken(user2.id(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users/${user1.id()}")).build())

        assert response.statusCode() == Response.Status.FORBIDDEN.statusCode
    }

    @Test
    void getUsers_0()
    {
        def response = restClient.get("/api/users")

        JSONAssert.assertEquals("""
        {
          "content": [],
          "number": 0,
          "size": 100,
          "totalElements": 0,
          "totalPages": 0
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_1()
    {
        def user1 = storeUser("user-1")

        def response = restClient.get("/api/users")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "id": "${user1.id()}",
              "name": "user-1",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": []
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 1,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_2()
    {
        def user1 = storeUser("user-1")
        def user2 = storeUser("user-2")

        // run twice to ensure there is no conflict between stream result and caching
        (1..2).forEach {
            def response = restClient.get("/api/users?sort=name:desc")

            JSONAssert.assertEquals("""
            {
              "content": [
                {
                  "id": "${user2.id()}",
                  "name": "user-2",
                  "totalWordCount": 0,
                  "followerCount": 0,
                  "phoneContacts": [],
                  "emailContacts": []
                },
                {
                  "id": "${user1.id()}",
                  "name": "user-1",
                  "totalWordCount": 0,
                  "followerCount": 0,
                  "phoneContacts": [],
                  "emailContacts": []
                }
              ],
              "number": 0,
              "size": 100,
              "totalElements": 2,
              "totalPages": 1
            }""", response.body(), JSONCompareMode.STRICT)
        }
    }

    @Test
    void getUsers_page()
    {
        def users = (0..9).collect { new UserIn("user$it", null, null,
                List.of(new EmailContact(EmailContact.Type.PRIVATE, "u$it@1.mail"),
                        new EmailContact(EmailContact.Type.PRIVATE, "u$it@2.mail")))
        }
        def userOuts = users.reverse()
                .collect { storeUser(it) }
                .reverse()

        def response = restClient.get("/api/users?sort=name&size=3&page=1")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "id": "${userOuts[3].id()}",
              "name": "${users[3].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[3].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[3].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[3].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[3].emailContacts()[1].emailAddress()}"
                }
              ]
            },
            {
              "id": "${userOuts[4].id()}",
              "name": "${users[4].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[4].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[4].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[4].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[4].emailContacts()[1].emailAddress()}"
                }
              ]
            },
            {
              "id": "${userOuts[5].id()}",
              "name": "${users[5].name()}",
              "totalWordCount": 0,
              "followerCount": 0,
              "phoneContacts": [],
              "emailContacts": [
                {
                  "type": "${users[5].emailContacts()[0].type().name()}",
                  "emailAddress": "${users[5].emailContacts()[0].emailAddress()}"
                },
                {
                  "type": "${users[5].emailContacts()[1].type().name()}",
                  "emailAddress": "${users[5].emailContacts()[1].emailAddress()}"
                }
              ]
            }
          ],
          "number": 1,
          "size": 3,
          "totalElements": 10,
          "totalPages": 4
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getUsers_unauthorized()
    {
        restClient.clearAuthToken()
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users")).build())

        assert response.statusCode() == Response.Status.UNAUTHORIZED.statusCode
    }

    @Test
    void getUsers_notPermitted()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER')
        def response = restClient.exchange(HttpRequest.newBuilder(baseUri.resolve("/api/users")).build())

        assert response.statusCode() == Response.Status.FORBIDDEN.statusCode
    }

    @Test
    void deleteUser()
    {
        def user1 = storeUser("user-1")
        (1..2).forEach { postNote(user1, "message $it") }

        def userUri = "/api/users/${user1.id()}"

        // check user is present
        restClient.get(userUri)

        def response = restClient.delete(userUri)

        assert response.statusCode()== Response.Status.NO_CONTENT.statusCode
        assert restClient.exchange(HttpRequest.newBuilder(baseUri.resolve(userUri)).build()).statusCode() == Response.Status.NOT_FOUND.statusCode
    }

    @Test
    void deleteUser_nonExistent()
    {
        def userUri = "/api/users/${UUID.randomUUID()}"

        // deleting non existent resource must not fail
        def response = restClient.delete(userUri)

        assert response.statusCode()== Response.Status.NO_CONTENT.statusCode
    }

    @Test
    void updateUser()
    {
        def user1 = storeUser("user-1", new PhoneContact(PhoneContact.Type.PRIVATE, "1234"))

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def userUri = "/api/users/${user1.id()}"

        restClient.put(userUri, '''
        {
          "name": "new-name",
          "phoneContacts": [],
          "emailContacts": [
            {
              "type": "PRIVATE",
              "emailAddress": "user1@home.com"
            },
            {
              "type": "COMPANY",
              "emailAddress": "user1@company.com"
            }
          ]
        }''')

        JSONAssert.assertEquals("""
        {
          "id": "${user1.id()}",
          "name": "new-name",
          "totalWordCount": 0,
          "followerCount": 0,
          "phoneContacts": [],
          "emailContacts": [
            {
              "type": "PRIVATE",
              "emailAddress": "user1@home.com"
            },
            {
              "type": "COMPANY",
              "emailAddress": "user1@company.com"
            }
          ]
        }""", restClient.get(userUri).body(), JSONCompareMode.STRICT)
    }
}
