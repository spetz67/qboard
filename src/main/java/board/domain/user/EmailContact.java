package board.domain.user;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public record EmailContact(@NotNull Type type, @NotEmpty String emailAddress)
{
    @Schema(name = "EmailContactType")
    public enum Type { PRIVATE, COMPANY }
}
