package board.application.management;

import board.application.JsonService;
import board.domain.common.Sequence;
import board.domain.follow.FollowRelation;
import board.domain.management.ManagementRepository;
import board.domain.management.MetaInfo;
import board.domain.management.RepositoryLock;
import board.domain.note.Note;
import board.domain.user.User;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.io.*;
import java.time.Instant;
import java.util.Iterator;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

@Singleton
@RequiredArgsConstructor
public class DataService
{
    private final JsonService jsonService;
    private final RepositoryLock repositoryLock;
    private final ManagementRepository<User> userManagementRepository;
    private final ManagementRepository<Note> noteManagementRepository;
    private final ManagementRepository<FollowRelation> followRelationManagementRepository;
    private final ManagementRepository<Sequence> sequenceManagementRepository;

    public void exportAsZip(OutputStream output) throws IOException
    {
        var zip = new ZipOutputStream(output);
        writeMetaInfo(zip);

        repositoryLock.withReadLock(() ->
        {
            writeAsNdJson(zip, "users.ndjson", userManagementRepository.getAll());
            writeAsNdJson(zip, "notes.ndjson", noteManagementRepository.getAll());
            writeAsNdJson(zip, "followRelations.ndjson", followRelationManagementRepository.getAll());
            writeAsNdJson(zip, "sequences.ndjson", sequenceManagementRepository.getAll());
        });

        zip.finish();
    }

    public void importFromZip(InputStream input) throws IOException
    {
        var zip = new ZipInputStream(input);
        var metaInfo = readMetaInfo(zip);
        if (!"board".equals(metaInfo.appName()))
            throw new IllegalArgumentException("Unexpected app name "+metaInfo.appName());
        if (metaInfo.modelVersion()!=1)
            throw new IllegalArgumentException("Unsupported model version "+metaInfo.modelVersion());

        repositoryLock.withWriteLock(() ->
        {
            sequenceManagementRepository.deleteAll();
            followRelationManagementRepository.deleteAll();
            noteManagementRepository.deleteAll();
            userManagementRepository.deleteAll();

            userManagementRepository.saveAll(ndJsonToStream(zip, "users.ndjson", User.class));
            noteManagementRepository.saveAll(ndJsonToStream(zip, "notes.ndjson", Note.class));
            followRelationManagementRepository.saveAll(ndJsonToStream(zip, "followRelations.ndjson", FollowRelation.class));
            sequenceManagementRepository.saveAll(ndJsonToStream(zip, "sequences.ndjson", Sequence.class));
        });
    }

    private void writeMetaInfo(ZipOutputStream zip) throws IOException
    {
        Properties buildInfo = new Properties();
        buildInfo.load(getClass().getResourceAsStream("/META-INF/build-info.properties"));
        String appVersion = buildInfo.getProperty("version");
        zip.putNextEntry(new ZipEntry("meta-info.json"));
        zip.write(jsonService.toJsonBytes(new MetaInfo("board", appVersion, 1, Instant.now())));
        zip.closeEntry();
    }

    private void writeAsNdJson(ZipOutputStream zip, String name, Stream<?> stream)
    {
        try
        {
            zip.putNextEntry(new ZipEntry(name));

            Iterator<?> iterator = stream.iterator();
            while (iterator.hasNext())
            {
                zip.write(jsonService.toJsonBytes(iterator.next()));
                zip.write('\n');
            }

            zip.closeEntry();
        }
        catch (IOException e)
        {
            throw new UncheckedIOException(e);
        }
    }

    private MetaInfo readMetaInfo(ZipInputStream zip) throws IOException
    {
        ZipEntry entry = Objects.requireNonNull(zip.getNextEntry());
        if (!"meta-info.json".equals(entry.getName()))
            throw new IllegalArgumentException("First entry is not meta-info.json, but "+entry.getName());

        MetaInfo metaInfo = jsonService.toObject(zip.readAllBytes(), MetaInfo.class);
        zip.closeEntry();
        return metaInfo;
    }

    private <T> Stream<T> ndJsonToStream(ZipInputStream zip, String name, Class<T> type)
    {
        try
        {
            ZipEntry entry = Objects.requireNonNull(zip.getNextEntry());
            if (!name.equals(entry.getName()))
                throw new IllegalArgumentException("Expected entry is not "+name+", but "+entry.getName());
        }
        catch (IOException e)
        {
            throw new UncheckedIOException("reading zip entry failed", e);
        }

        return new BufferedReader(new InputStreamReader(zip)).lines().map(line -> jsonService.toObject(line, type));
    }
}
