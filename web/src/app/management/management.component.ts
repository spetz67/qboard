import {Component, OnInit} from '@angular/core';
import {Location} from "@angular/common";
import {ManagementService} from "@board-api/services/management.service";

@Component({
    selector: 'app-management',
    templateUrl: './management.component.html',
    styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {
    version: ''
    selectedFile: File
    uploading: boolean

    constructor(private location: Location,
                private managementService: ManagementService) {
    }

    ngOnInit(): void {
        this.managementService
                .info()
                .subscribe(info => this.version = info['version']);
    }

    goBack() {
        this.location.back()
    }

    fileSelected(file: File) {
        this.selectedFile = file
    }

    upload() {
        this.uploading = true
        this.managementService.importData({ body: this.selectedFile })
                .subscribe(() => this.uploading = false )
        this.selectedFile = null
    }
}
