package board.port.required.auth;

import board.application.AuthQueryRepository;
import board.port.required.CacheKeys;
import board.port.required.jooq.Tables;
import board.port.required.jooq.tables.UserEmailContact;
import io.quarkus.cache.CacheResult;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
class AuthQueryRepositoryImpl implements AuthQueryRepository
{
    private final DSLContext jooq;

    @Override
    @Transactional
    @CacheResult(cacheName = CacheKeys.USER)
    public Optional<UUID> findUserIdByEmail(String emailAddress)
    {
        return jooq.select(UserEmailContact.USER_EMAIL_CONTACT.USER_ID)
                .from(Tables.USER_EMAIL_CONTACT)
                .where(Tables.USER_EMAIL_CONTACT.EMAIL_ADDRESS.eq(emailAddress))
                .fetchOptional(UserEmailContact.USER_EMAIL_CONTACT.USER_ID);
    }
}