-- Use compound primary keys instead of artificial string ids

alter table "note"
    drop column "id",
    add primary key ("user_id", "number");

alter table "follow_relation"
    drop column "id",
    add primary key ("follower_user_id", "followee_user_id");

