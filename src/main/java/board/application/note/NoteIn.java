package board.application.note;

import board.domain.note.Media;

import javax.validation.constraints.NotBlank;
import java.util.List;

public record NoteIn(

        @NotBlank
        String text,

        List<Media> media
)
{
}
