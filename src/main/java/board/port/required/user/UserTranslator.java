package board.port.required.user;

import board.application.JsonService;
import board.application.user.UserOut;
import board.domain.user.EmailContact;
import board.domain.user.PhoneContact;
import board.domain.user.User;
import board.port.required.jooq.Tables;
import board.port.required.jooq.tables.records.UserEmailContactRecord;
import board.port.required.jooq.tables.records.UserRecord;
import board.port.required.jooq.tables.records.UserViewRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.JSON;

import javax.annotation.Nullable;
import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
class UserTranslator
{
    private final JsonService jsonService;

    User toUser(UserRecord record, List<EmailContact> emailContacts)
    {
        return new User(record.getId(), record.getRevisionNumber(), record.getCreatedInstant(),
                record.getUpdatedInstant(), record.getName(), record.getNotificationEmailAddress(),
                record.getTotalWordCount(), jsonToList(record.getPhoneContactsJson(), PhoneContact.class),
                emailContacts);
    }

    User toUser(UserViewRecord record)
    {
        return new User(record.getId(), record.getRevisionNumber(), record.getCreatedInstant(),
                record.getUpdatedInstant(), record.getName(),
                record.getNotificationEmailAddress(),
                Objects.requireNonNullElse(record.getTotalWordCount(), 0),
                jsonToList(record.getPhoneContactsJson(), PhoneContact.class),
                jsonToList(record.getEmailContactsJson(), EmailContact.class));
    }

    UserOut toUserOut(UserViewRecord record)
    {
        return new UserOut(record.getId(), record.getUpdatedInstant(),
                Objects.requireNonNullElse(record.getTotalWordCount(), 0),
                Objects.requireNonNullElse(record.getFollowerCount(), 0), record.getName(),
                record.getNotificationEmailAddress(),
                asString(record.getPhoneContactsJson()), asString(record.getEmailContactsJson()));
    }

    EmailContact toEmailContact(UserEmailContactRecord record)
    {
        return new EmailContact(EmailContact.Type.valueOf(record.getType()), record.getEmailAddress());
    }

    UserRecord toRecord(User user)
    {
        return new UserRecord(user.getId(), user.getRevisionNumber(), user.getCreatedInstant(),
                user.getUpdatedInstant(), user.getName(), user.getTotalWordCount(),
                JSON.valueOf(jsonService.toJson(user.getPhoneContacts())), user.getNotificationEmailAddress());
    }

    UserEmailContactRecord toNewRecord(EmailContact ec, @NotNull UUID userId)
    {
        var record = new UserEmailContactRecord(null, userId, ec.type().name(), ec.emailAddress());
        record.reset(Tables.USER_EMAIL_CONTACT.ID);
        return record;
    }

    private <T> List<T> jsonToList(@Nullable JSON json, Class<T> type)
    {
        if (json==null)
            return null;

        return jsonService.toList(json.data(), type);
    }

    private String asString(@Nullable JSON json)
    {
        if (json==null)
            return null;

        return json.data();
    }
}
