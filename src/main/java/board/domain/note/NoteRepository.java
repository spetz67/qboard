package board.domain.note;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

public abstract class NoteRepository
{
    protected abstract Optional<Note> findById(@Valid Note.Id id);

    protected abstract void add(@Valid Note note);

    protected abstract void deleteById(@Valid Note.Id id);

    protected abstract int getNoteCountByUserId(@NotNull UUID userId);

    protected abstract void deleteAllByUserId(@NotNull UUID userId);
}