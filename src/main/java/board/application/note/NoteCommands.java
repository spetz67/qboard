package board.application.note;

import board.domain.common.RevisionMismatchException;
import board.domain.note.NotesExceededException;
import board.domain.user.UserNotFoundException;
import board.domain.note.Note;
import board.domain.note.NoteConfiguration;
import board.domain.note.NoteFactory;
import board.domain.note.NoteRepoService;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.faulttolerance.Retry;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class NoteCommands
{
    private final NoteRepoService repoService;
    private final NoteFactory factory;
    private final NoteConfiguration configuration;

    @Retry(retryOn = RevisionMismatchException.class)
    public Note postNote(UUID userId, NoteIn noteIn) throws UserNotFoundException, NotesExceededException
    {
        int noteCount = repoService.getNoteCountByUserId(userId);
        if (configuration.exceedsMaxNotes(noteCount+1))
            throw new NotesExceededException(configuration.getMaxNotesPerUser());

        Note note = factory.createNote(userId, noteIn.text(), noteIn.media());
        repoService.add(note);
        return note;
    }

    @Retry(retryOn = RevisionMismatchException.class)
    public void deleteNoteByUserAndNumber(UUID userId, int noteNumber)
    {
        repoService.delete(userId, noteNumber);
    }
}