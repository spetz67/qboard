package board.port.provided;

import board.domain.common.RevisionMismatchException;
import board.domain.follow.FollowRelationNotFoundException;
import board.domain.note.NoteNotFoundException;
import board.domain.user.UserNotFoundException;
import board.domain.common.TemplatedException;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.*;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

@Provider
@Slf4j
public class TemplatedExceptionHandler implements ExceptionMapper<TemplatedException>
{
    private final static Map<Class<? extends Throwable>, Response.Status> HTTP_STATUS_MAP = Map.of(
            UserNotFoundException.class, Response.Status.NOT_FOUND,
            NoteNotFoundException.class, Response.Status.NOT_FOUND,
            FollowRelationNotFoundException.class, Response.Status.NOT_FOUND,
            RevisionMismatchException.class, Response.Status.CONFLICT
    );

    private final UriInfo uriInfo;
    private final Request request;
    private final HttpHeaders headers;

    @Value
    private static class JsonError
    {
        private String message;
        private String path;
    }

    public TemplatedExceptionHandler(@Context UriInfo uriInfo, @Context Request request, @Context HttpHeaders headers)
    {
        this.uriInfo = uriInfo;
        this.request = request;
        this.headers = headers;
    }

    @Override
    public Response toResponse(TemplatedException e)
    {
        Locale locale = headers.getAcceptableLanguages().get(0);
        ResourceBundle res = ResourceBundle.getBundle("board.port.provided.ErrorRes", locale);
        String template = res.getString(e.getCode());
        String message = expandVariables(template, e.getVariables());

        JsonError error = new JsonError(message, uriInfo.getPath());

        Response.Status httpStatus = HTTP_STATUS_MAP.getOrDefault(e.getClass(), Response.Status.BAD_REQUEST);

        log.error("Error response {} ({}) on {} {}; {}",
                httpStatus.getStatusCode(), httpStatus.getReasonPhrase(), request.getMethod(), uriInfo.getRequestUri(), message);

        return Response
                .status(httpStatus)
                .type(MediaType.APPLICATION_JSON)
                .entity(error)
                .build();
    }

    private String expandVariables(String template, Map<String, Object> variables)
    {
        for (Map.Entry<String, Object> entry : variables.entrySet())
            template = template.replace('{'+entry.getKey()+'}', String.valueOf(entry.getValue()));

        return template;
    }
}
