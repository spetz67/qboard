package board.port.provided.auth;

import org.eclipse.microprofile.config.inject.ConfigProperties;

import javax.enterprise.inject.Instance;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;
import java.security.Principal;

/**
 * Configurable authorization filter that allows to define ant style path patterns (with optional method)
 * to be permitted. The wildcard @PRINCIPAL@ is replaced by the logged in principal name.
 *
 * Default quarkus configuration only allows to restrict access more and more with each entry
 * and just a wildcard at the end of the path.
 */
@Provider
public class AuthorizationFilter implements ContainerRequestFilter
{
    @ConfigProperties
    Instance<SecurityConfig> securityConfig;

    @Override
    public void filter(ContainerRequestContext context)
    {
        var matchedEntry = securityConfig.get().getInterceptUrlMap().stream()
                .filter(entry -> methodAndPathMatch(context, entry))
                .findFirst();

        if (matchedEntry.isPresent() && accessMatches(context, matchedEntry.get()))
            return;     // pass request

        Response.Status status = context.getSecurityContext().getUserPrincipal()!=null
                ? Response.Status.FORBIDDEN
                : Response.Status.UNAUTHORIZED;
        context.abortWith(Response.status(status).build());
    }

    private boolean methodAndPathMatch(ContainerRequestContext context, SecurityConfig.InterceptUrlMapEntry entry)
    {
        if (entry.httpMethod()
                .map(method -> !method.equals(context.getMethod()))
                .orElse(false))
            return false;

        String path = context.getUriInfo().getPath();
        String regex = entry.pattern()
                .replace("/**", "@ANY_PATH@")
                .replace("*", "[^/]*")
                .replace("@ANY_PATH@", "($|/.*)");

        Principal principal = context.getSecurityContext().getUserPrincipal();
        if (principal!=null)
            regex = regex.replace("@PRINCIPAL@", principal.getName());

        return path.matches(regex);
    }

    private boolean accessMatches(ContainerRequestContext requestContext, SecurityConfig.InterceptUrlMapEntry entry)
    {
        String access = entry.access();
        SecurityContext securityContext = requestContext.getSecurityContext();
        if ("isAnonymous()".equals(access))
            return true;
        if ("isAuthenticated()".equals(access))
            return securityContext.getUserPrincipal()!=null;
        return securityContext.isUserInRole(access);
    }
}
