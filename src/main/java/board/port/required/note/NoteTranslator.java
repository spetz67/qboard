package board.port.required.note;

import board.application.JsonService;
import board.domain.note.Media;
import board.domain.note.Note;
import board.port.required.jooq.tables.records.NoteRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.JSON;
import org.jooq.RecordMapper;
import org.jooq.RecordUnmapper;

import javax.inject.Singleton;
import java.util.List;

@Singleton
@RequiredArgsConstructor
class NoteTranslator implements RecordMapper<NoteRecord, Note>, RecordUnmapper<Note, NoteRecord>
{
    private final JsonService jsonService;

    @Override
    public Note map(NoteRecord record)
    {
        List<Media> media = record.getMediaJson()!=null
                ? jsonService.toList(record.getMediaJson().data(), Media.class)
                : null;
        return new Note(record.getUserId(), record.getNumber(), record.getInstant(), record.getText(), media);
    }

    @Override
    public NoteRecord unmap(Note note)
    {
        JSON mediaJson = note.media()!=null
                ? JSON.valueOf(jsonService.toJson(note.media()))
                : null;
        return new NoteRecord(note.userId(), note.number(), note.instant(), note.text(), mediaJson);
    }
}
