package board.domain.management;

public interface RepositoryLock
{
    void withReadLock(Runnable runnable);

    void withWriteLock(Runnable runnable);
}
