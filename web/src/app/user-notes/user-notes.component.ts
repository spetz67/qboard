import {Component} from '@angular/core';
import {FormControl} from "@angular/forms";
import {NoteOut, Media, NoteIn} from "@board-api/models";
import {NotesService} from "@board-api/services";
import {ActivatedRoute} from "@angular/router";

@Component({
    selector: 'app-user-notes',
    templateUrl: './user-notes.component.html',
    styleUrls: ['./user-notes.component.css']
})
export class UserNotesComponent {

    newNoteTextArea: FormControl = new FormControl()

    notes: NoteOut[] = [];
    imageSrc: string | ArrayBuffer = null;

    private userId: string;
    private attachmentFile: Blob;

    constructor(private notesService: NotesService,
                private route: ActivatedRoute) {

        this.userId = this.route.snapshot.data.userId
        this.notes = this.route.snapshot.data.notes;
    }

    postNote() {
        let file = this.attachmentFile;
        let noteIn: NoteIn = { text: this.newNoteTextArea.value }
        if (file) {
            let fileReader = new FileReader();
            fileReader.onload = event => {
                noteIn.media = [{mediaType: file.type, content: btoa(event.target.result as string)}]
                this.postNoteIn(noteIn);
            }
            fileReader.readAsBinaryString(file)
        }
        else
            this.postNoteIn(noteIn);
    }

    deleteNote(number: number) {
        this.notesService
                .deleteNoteByNumber({userId: this.userId, number: number})
                .subscribe(() => this.loadNotes())
    }

    fileSelected(file: File) {
        let fileReader = new FileReader();
        fileReader.onload = event => this.imageSrc = event.target.result
        fileReader.readAsDataURL(file)
    }

    removeImage() {
        this.imageSrc = null
        this.attachmentFile = null
    }

    imageLoaded(imageElement: HTMLImageElement, event: Event) {
        let canvas = document.createElement('canvas')
        canvas.width = imageElement.width
        canvas.height = imageElement.height
        let context = canvas.getContext("2d");
        context.drawImage(imageElement, 0, 0, canvas.width,  canvas.height)
        canvas.toBlob(blob => this.attachmentFile = blob, "image/jpeg")
    }

    private postNoteIn(noteIn: NoteIn) {
        this.notesService
                .postNote({ userId: this.userId, body: noteIn})
                .subscribe(value => {
                    this.newNoteTextArea.setValue('');
                    this.removeImage()
                    this.loadNotes()
                })
    }

    private loadNotes() {
        this.notesService
                .getNotes({ userId: this.userId, sort: ['number:desc'] })
                .subscribe(notes => this.notes = notes.content)
    }
}
