package board.port.required.notification;

import board.domain.notification.Notification;
import board.port.required.jooq.Tables;
import board.port.required.jooq.tables.records.NotificationRecord;

import javax.inject.Singleton;

@Singleton
class EntityTranslator
{
    Notification toNotification(NotificationRecord record)
    {
        return new Notification(record.getId(), record.getLocked(), record.getRecipient(),
                record.getSubject(), record.getContent());
    }

    NotificationRecord toNewRecord(Notification notification)
    {
        NotificationRecord record = new NotificationRecord(null, notification.locked(),
                notification.recipient(), notification.subject(), notification.content());
        record.reset(Tables.NOTIFICATION.ID);
        return record;
    }
}
