package board.domain.common;

import java.util.Map;

public class RevisionMismatchException extends TemplatedException
{
    public RevisionMismatchException(int version)
    {
        super("error.revisionMismatch", Map.of("version", version));
    }
}
