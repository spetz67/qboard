package board.domain.common;

import javax.enterprise.event.Observes;
import java.util.ArrayList;
import java.util.List;

public class TestEventReceiver<E>
{
    private final List<E> events = new ArrayList<>();

    public void reset()
    {
        events.clear();
    }

    public List<E> getEvents()
    {
        return events;
    }

    public void onEvent(@Observes E event)
    {
        events.add(event);
    }
}
