package board.domain.notification;

import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Singleton
@RequiredArgsConstructor
class NotificationFactory
{
    private final NotificationConfiguration notificationConfiguration;

    Notification create(String recipient, String subject, String templateResource, Map<String, ?> variables)
    {
        try
        {
            String template = new String(Objects.requireNonNull(getClass().getResourceAsStream(templateResource)).readAllBytes());

            Map<String, Object> extendedVariables = new HashMap<>(Map.of(
                    "url", notificationConfiguration.getBoardUrl()
            ));
            extendedVariables.putAll(variables);

            String content = expandVariables(template, extendedVariables);
            return new Notification(null, false, recipient, subject, content);
        }
        catch (IOException e)
        {
            throw new UncheckedIOException("Creating notification from template failed", e);
        }
    }

    private String expandVariables(String template, Map<String, ?> variables)
    {
        for (Map.Entry<String, ?> entry : variables.entrySet())
            template = template.replace("${"+entry.getKey()+'}', String.valueOf(entry.getValue()));

        return template;
    }
}
