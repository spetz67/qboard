package board.port.required.common;

import board.domain.common.SequenceRepository;
import board.port.required.jooq.tables.records.SequenceRecord;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;

import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.util.UUID;

import static board.port.required.jooq.tables.Sequence.SEQUENCE;

@RequiredArgsConstructor
@Singleton
class SequenceRepositoryImpl implements SequenceRepository
{
    private final DSLContext jooq;

    @Override
    @Transactional
    public int getNext(UUID id)
    {
        int number = jooq.select(SEQUENCE.NUMBER)
                .from(SEQUENCE)
                .where(SEQUENCE.ID.eq(id))
                .forUpdate()
                .fetchOptional(SEQUENCE.NUMBER)
                .orElseThrow();
        jooq.update(SEQUENCE)
                .set(SEQUENCE.NUMBER, number+1)
                .where(SEQUENCE.ID.eq(id))
                .execute();
        return number;
    }

    @Override
    @Transactional
    public void addSequence(UUID id)
    {
        jooq.executeInsert(new SequenceRecord(id, 1));
    }

    @Override
    public void deleteById(UUID id)
    {
        jooq.deleteFrom(SEQUENCE)
                .where(SEQUENCE.ID.eq(id))
                .execute();
    }
}