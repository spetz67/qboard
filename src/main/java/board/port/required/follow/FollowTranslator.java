package board.port.required.follow;

import board.domain.follow.FollowRelation;
import board.domain.follow.Followee;
import board.domain.follow.Follower;
import board.port.required.jooq.tables.records.FollowRelationRecord;
import org.jooq.RecordMapper;
import org.jooq.RecordUnmapper;

import javax.inject.Singleton;

@Singleton
class FollowTranslator implements RecordMapper<FollowRelationRecord, FollowRelation>, RecordUnmapper<FollowRelation, FollowRelationRecord>
{
    @Override
    public FollowRelation map(FollowRelationRecord record)
    {
        return new FollowRelation(record.getCreatedInstant(),
                new Follower(record.getFollowerUserId(), record.getFollowerUserName()),
                new Followee(record.getFolloweeUserId(), record.getFolloweeUserName(),
                        record.getFolloweeFirstUnreadNoteNumber()));
    }

    @Override
    public FollowRelationRecord unmap(FollowRelation followRelation)
    {
        Follower follower = followRelation.getFollower();
        Followee followee = followRelation.getFollowee();
        return new FollowRelationRecord(followRelation.getCreatedInstant(),
                follower.userId(), follower.userName(),
                followee.userId(), followee.userName(), followee.firstUnreadNoteNumber());
    }
}