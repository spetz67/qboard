package board.domain.management;

import java.time.Instant;

public record MetaInfo(String appName, String appVersion, int modelVersion, Instant createdInstant)
{
}
