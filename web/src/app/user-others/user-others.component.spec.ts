import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserOthersComponent } from './user-others.component';

describe('UserOthersComponent', () => {
  let component: UserOthersComponent;
  let fixture: ComponentFixture<UserOthersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserOthersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserOthersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
