package board.port.provided;

import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeIn;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import javax.ws.rs.core.Application;

@OpenAPIDefinition(info = @Info(title = "Board", description = "Board API", version = "1.0"))
@SecurityScheme(securitySchemeName = "bearerAuth",
        in = SecuritySchemeIn.COOKIE,
        type = SecuritySchemeType.APIKEY,
        apiKeyName = "access-token",
        scheme = "bearer",
        bearerFormat = "jwt"
)
public class BoardApplication extends Application
{
}