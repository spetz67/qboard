package board.domain.note;

import board.domain.common.TemplatedException;

import java.util.Map;

public class NotesExceededException extends TemplatedException
{
    public NotesExceededException(int maxNotesNumber)
    {
        super("error.user.maxNumberOfNotesExceeded", Map.of("maxNotesNumber", maxNotesNumber));
    }
}
