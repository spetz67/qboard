-- Change json columns from byte array to json

drop view "user_view";

alter table "user"
    alter column "phone_contacts_json" type json using (convert_from("phone_contacts_json", 'UTF-8')::json);

create view "user_view" as
select *,
       (select count(*) from "follow_relation" where "followee_user_id" = user_view."id")::int as follower_count,
       (select coalesce(json_agg(json_build_object('type', "type", 'emailAddress', "email_address")), json_build_array())
        from "user_email_contact"
        where "user_id" = user_view."id") as email_contacts_json
from "user" user_view
