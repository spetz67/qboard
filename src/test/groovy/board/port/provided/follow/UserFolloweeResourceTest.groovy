package board.port.provided.follow


import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.Response

@QuarkusTest
@CompileStatic
class UserFolloweeResourceTest extends AbstractResourceTest
{
    @Test
    void getFollowees_2()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        def followee1 = follow(user1, user2.id())
        def followee2 = follow(user1, user3.id())

        def response = restClient.get("/api/users/${user1.id()}/followees")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "userId": "${user2.id()}",
              "createdInstant": "${followee1.createdInstant()}",
              "newNoteAvailable": false,
              "userName": "user2"
            },
            {
              "userId": "${user3.id()}",
              "createdInstant": "${followee2.createdInstant()}",
              "newNoteAvailable": false,
              "userName": "user3"
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.NON_EXTENSIBLE)
    }

    @Test
    void getFollowee()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        def user3 = storeUser("user3")
        def followee1 = follow(user1, user2.id())
        follow(user1, user3.id())
        postNote(user2, "message")

        def response = restClient.get("/api/users/${user1.id()}/followees/${user2.id()}")

        JSONAssert.assertEquals("""
        {
            "userId": "${user2.id()}",
            "createdInstant": "${followee1.createdInstant()}",
            "newNoteAvailable": true,
            "firstUnreadNoteNumber": 1,
            "userName": "user2"
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void follow()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")

        def request = "{}"

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def followeesUri = "/api/users/${user1.id()}/followees"
        def response = restClient.post("$followeesUri/${user2.id()}", request)

        assert response.statusCode() == Response.Status.CREATED.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == null
        assert response.headers().firstValue(HttpHeaders.LOCATION).orElse(null) == baseUri.resolve("$followeesUri/${user2.id()}").toString()
        assert response.body() == ''
    }

    @Test
    void unfollow()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        follow(user1, user2.id())

        def followeesUri = "/api/users/${user1.id()}/followees"

        // ensure following was successful
        restClient.get("$followeesUri/${user2.id()}")

        restClient.delete("$followeesUri/${user2.id()}")

        def followeesResponse = restClient.get("$followeesUri")
        JSONAssert.assertEquals("""
        {
          "content": [],
          "number": 0,
          "size": 100,
          "totalElements": 0,
          "totalPages": 0
        }""", followeesResponse.body(), JSONCompareMode.STRICT)
    }

    @Test
    void resetNewNoteAvailable()
    {
        def user1 = storeUser("user1")
        def user2 = storeUser("user2")
        follow(user1, user2.id())
        postNote(user2, "message")

        def followeesUri = "/api/users/${user1.id()}/followees"

        // ensure post caused newNoteAvailable=true
        def followeeResponse = restClient.get("$followeesUri/${user2.id()}")
        JSONAssert.assertEquals("""
        {
            "newNoteAvailable": true
        }""", followeeResponse.body(), JSONCompareMode.LENIENT)

        restClient.patch("$followeesUri/${user2.id()}", """{
            "newNoteAvailable": false
        }""")

        followeeResponse = restClient.get("$followeesUri/${user2.id()}")
        JSONAssert.assertEquals("""
        {
            "newNoteAvailable": false
        }""", followeeResponse.body(), JSONCompareMode.LENIENT)
    }
}
