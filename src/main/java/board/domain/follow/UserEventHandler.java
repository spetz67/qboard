package board.domain.follow;

import board.domain.note.Note;
import board.domain.note.NotePostedEvent;
import board.domain.user.BeforeUserDeletedEvent;
import board.domain.user.UserNameChangedEvent;
import lombok.RequiredArgsConstructor;

import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
class UserEventHandler
{
    private final FollowRelationRepository followRelationRepository;
    private final Event<FolloweePostedNoteEvent> eventService;

    public void setNewNoteAvailableOnFollowees(@Observes NotePostedEvent event)
    {
        Note note = event.note();
        var followRelations = followRelationRepository.getByFollowee(note.userId()).toList();

        for (FollowRelation followRelation : followRelations)
        {
            followRelation.newNoteAvailableFromFollowee(note);
            followRelationRepository.update(followRelation);
        }

        eventService.fire(new FolloweePostedNoteEvent(followRelations, note));
    }

    public void deleteRelatedFollowRelations(@Observes BeforeUserDeletedEvent event)
    {
        UUID userId = event.userId();

        followRelationRepository
                .getByFollower(userId)
                .forEach(followRelation -> followRelationRepository.deleteById(followRelation.getId()));

        followRelationRepository
                .getByFollowee(userId)
                .forEach(followRelation -> followRelationRepository.deleteById(followRelation.getId()));
    }

    public void updateFollowRelationUserNames(@Observes UserNameChangedEvent event)
    {
        UUID userId = event.user().getId();

        followRelationRepository
                .getByFollower(userId)
                .forEach(followRelation -> {
                    followRelation.setFollowerName(event.user().getName());
                    followRelationRepository.update(followRelation);
                });

        followRelationRepository
                .getByFollowee(userId)
                .forEach(followRelation -> {
                    followRelation.setFolloweeName(event.user().getName());
                    followRelationRepository.update(followRelation);
                });
    }
}
