package board.port.required.note;

import board.domain.note.NoteConfiguration;
import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.inject.Singleton;

@Singleton
class NoteConfigurationImpl implements NoteConfiguration
{
    @Getter
    @ConfigProperty(name = "board.note.max-notes-per-user", defaultValue = "100")
    Integer maxNotesPerUser;
}
