package board.port.required.management;

import board.domain.management.ManagementRepository;
import board.port.required.jooq.Public;
import org.jooq.*;

import javax.inject.Inject;
import java.lang.reflect.ParameterizedType;
import java.util.stream.Stream;

public abstract class AbstractManagementRepository<E, R extends TableRecord<R>> implements ManagementRepository<E>
{
    @Inject
    DSLContext jooq;

    @Inject
    RecordMapper<R, E> recordMapper;

    @Inject
    RecordUnmapper<E, R> recordUnmapper;

    private final Table<R> table = getTable();

    @Override
    public Stream<E> getAll()
    {
        return jooq.selectFrom(table)
                .fetchStream()
                .map(recordMapper);
    }

    @Override
    public void deleteAll()
    {
        jooq.deleteFrom(table).execute();
    }

    @Override
    public void saveAll(Stream<E> entities)
    {
        entities.forEach(e -> jooq.executeInsert(recordUnmapper.unmap(e)));
    }

    @SuppressWarnings("unchecked")
    protected Table<R> getTable()
    {
        for (Class<?> c = getClass(); c!=null; c = c.getSuperclass())
        {
            if (c.getGenericSuperclass() instanceof ParameterizedType pt)
            {
                var recordTypeName = pt.getActualTypeArguments()[1].getTypeName();
                return (Table<R>) Public.PUBLIC.getTables().stream()
                        .filter(table -> table.getRecordType().getTypeName().equals(recordTypeName))
                        .findAny()
                        .orElseThrow();
            }
        }
        throw new IllegalStateException("type parameter not found");
    }
}
