package board.domain.user;

import board.application.Pageable;
import board.application.note.NoteQueryRepository;
import board.domain.common.RevisionMismatchException;
import board.port.required.TestRepository;
import io.quarkus.test.junit.QuarkusTest;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.time.Instant;
import java.util.List;

@QuarkusTest
public class UserRepoServiceTest
{
    @Inject
    UserRepoService userRepoService;

    @Inject
    NoteQueryRepository noteQueries;

    @Inject
    UserFactory userFactory;

    @Inject
    TestRepository testRepository;

    @AfterEach
    void tearDown()
    {
        testRepository.deleteAll();
    }

    @Test
    void add()
    {
        Instant beforeCreate = Instant.now();
        User user = user("test-user");

        Instant beforeSave = Instant.now();
        userRepoService.add(user);
        Instant afterSave = Instant.now();

        Assertions.assertNotNull(user.getUpdatedInstant());
        Assertions.assertEquals(1, user.getRevisionNumber());

        User storedUser = userRepoService.getById(user.getId());

        compareWithoutTimeStamps(user, storedUser);
        assertInstant(storedUser.getCreatedInstant(), beforeCreate, beforeSave);
        assertInstant(storedUser.getUpdatedInstant(), beforeSave, afterSave);

        Assertions.assertThrows(org.jooq.exception.DataAccessException.class, () -> userRepoService.add(user));
    }

    @Test
    void add_existing()
    {
        User user = user("test-user");
        userRepoService.add(user);
        User storedUser = userRepoService.getById(user.getId());

        Assertions.assertThrows(org.jooq.exception.DataAccessException.class, () -> userRepoService.add(storedUser));
    }

    @Test
    void update()
    {
        Instant beforeCreate = Instant.now();
        User user = user("test-user");
        Instant afterCreate = Instant.now();

        userRepoService.add(user);

        user.setName("new-name");
        user.setNotificationEmailAddress("new-notification-email-address@mail.com");
        user.setPhoneContacts(List.of(
                new PhoneContact(PhoneContact.Type.COMPANY, "0123"),
                new PhoneContact(PhoneContact.Type.PRIVATE, "0125")
        ));
        user.setEmailContacts(List.of(
                new EmailContact(EmailContact.Type.COMPANY, "a@b.c"),
                new EmailContact(EmailContact.Type.PRIVATE, "m1@n.o")
        ));

        Instant beforeUpdate = Instant.now();
        userRepoService.update(user);
        Instant afterUpdate = Instant.now();

        User storedUser = userRepoService.getById(user.getId());

        compareWithoutTimeStamps(user, storedUser);
        assertInstant(storedUser.getCreatedInstant(), beforeCreate, afterCreate);
        assertInstant(storedUser.getUpdatedInstant(), beforeUpdate, afterUpdate);
    }

    @Test
    void update_removeEmailContacts()
    {
        User user = user("test-user");
        userRepoService.add(user);

        user.setName("new-name");
        user.setEmailContacts(List.of());

        userRepoService.update(user);

        User storedUser = userRepoService.getById(user.getId());

        compareWithoutTimeStamps(user, storedUser);
    }

    @Test
    void delete()
    {
        User user = user("test-user");
        userRepoService.add(user);

        userRepoService.delete(user.getId());

        Assertions.assertThrows(UserNotFoundException.class, () -> userRepoService.getById(user.getId()));
        Assertions.assertEquals(0,
                noteQueries.queryNotesByUserId(user.getId(), null, Pageable.unpaged()).totalSize());

        userRepoService.delete(user.getId());    // repeated call must succeed
    }

    @Test
    void update_revisionMismatch()
    {
        User user = user("test-user");
        userRepoService.add(user);
        User storedUser = userRepoService.getById(user.getId());

        userRepoService.update(user);
        Assertions.assertThrows(RevisionMismatchException.class, () -> userRepoService.update(storedUser));
    }

    @Test
    void getUserById()
    {
        User user = user("test-user");
        userRepoService.add(user);

        User storedUser = userRepoService.getById(user.getId());
        Assertions.assertEquals(user.getId(), storedUser.getId());
        Assertions.assertEquals(1, storedUser.getRevisionNumber());
    }

    private User user(String name)
    {
        User user = userFactory.createUser(name);
        user.setNotificationEmailAddress(name+"@mail.com");
        user.setPhoneContacts(List.of(
                new PhoneContact(PhoneContact.Type.COMPANY, "0123"),
                new PhoneContact(PhoneContact.Type.MOBILE, "0124")
        ));
        user.setEmailContacts(List.of(
                new EmailContact(EmailContact.Type.COMPANY, "a@b.c"),
                new EmailContact(EmailContact.Type.PRIVATE, "x@y.z")
        ));
        return user;
    }

    private void compareWithoutTimeStamps(User user, User storedUser)
    {
        Assertions.assertEquals(user.getId(), storedUser.getId());
        Assertions.assertEquals(user.getRevisionNumber(), storedUser.getRevisionNumber());
        Assertions.assertEquals(user.getName(), storedUser.getName());
        Assertions.assertEquals(user.getNotificationEmailAddress(), storedUser.getNotificationEmailAddress());
        Assertions.assertEquals(user.getEmailContacts(), storedUser.getEmailContacts());
        Assertions.assertEquals(user.getPhoneContacts(), storedUser.getPhoneContacts());
        Assertions.assertEquals(user.getTotalWordCount(), storedUser.getTotalWordCount());
    }

    private void assertInstant(Instant instant, Instant notBefore, Instant notAfter)
    {
        MatcherAssert.assertThat(instant, Matchers.allOf(
                Matchers.greaterThanOrEqualTo(notBefore), Matchers.lessThanOrEqualTo(notAfter)));
    }
}
