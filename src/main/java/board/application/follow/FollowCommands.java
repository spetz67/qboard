package board.application.follow;

import board.domain.follow.FollowRelation;
import board.domain.follow.FollowRelationFactory;
import board.domain.follow.FollowRepoService;
import lombok.RequiredArgsConstructor;

import javax.inject.Singleton;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class FollowCommands
{
    private final FollowRepoService repoService;
    private final FollowRelationFactory factory;

    public void follow(UUID userId, UUID followeeUserId)
    {
        FollowRelation followRelation = factory.createFollowRelation(userId, followeeUserId);
        repoService.add(followRelation);
    }

    public void unfollow(UUID userId, UUID followeeUserId)
    {
        repoService.delete(userId, followeeUserId);
    }

    public void resetFolloweeFirstUnreadNoteNumber(UUID userId, UUID followeeUserId)
    {
        FollowRelation followRelation = repoService.get(userId, followeeUserId);
        followRelation.resetFolloweeFirstUnreadNoteNumber();
        repoService.update(followRelation);
    }
}