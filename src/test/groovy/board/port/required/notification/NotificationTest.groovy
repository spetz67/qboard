package board.port.required.notification

import board.application.follow.FollowCommands
import board.application.follow.FollowQueryRepository
import board.application.note.NoteCommands
import board.application.note.NoteIn
import board.application.user.UserCommands
import board.application.user.UserIn
import board.application.user.UserQueryRepository
import board.port.required.TestRepository
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.awaitility.Awaitility
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.subethamail.wiser.Wiser

import javax.inject.Inject

@QuarkusTest
@CompileStatic
class NotificationTest
{
    @Inject UserCommands userCommands
    @Inject NoteCommands noteCommands
    @Inject FollowCommands followCommands
    @Inject UserQueryRepository userQueries
    @Inject FollowQueryRepository followQueries
    @Inject TestRepository testRepository

    private Wiser wiser

    @BeforeEach
    void setUp() {

        wiser = new Wiser(5025)
        wiser.start()

        testRepository.deleteAll()
    }

    @AfterEach
    void tearDown() {

        wiser.stop()
    }

    @Test
    void followeePostedNote() {

        def userId1 = userCommands.registerUser(new UserIn("user1", "user1@board.com", null, List.of()))
        def userId2 = userCommands.registerUser(new UserIn("user2", "user2@board.com", null, List.of()))
        def userId3 = userCommands.registerUser(new UserIn("user3", null, null, List.of()))
        followCommands.follow(userId1, userId2)
        followCommands.follow(userId1, userId3)
        followCommands.follow(userId2, userId3)

        noteCommands.postNote(userId2, new NoteIn("message2", null))
        noteCommands.postNote(userId3, new NoteIn("message3", null))

        Awaitility.await().until { wiser.messages.size()>=3 }
        assert wiser.messages.size()==3

        // follow relation order is not defined in database and therefore send order is undefined -> sort by receiver
        wiser.messages.sort { m1, m2 -> m1.envelopeReceiver.compareTo(m2.envelopeReceiver) }

        def message1 = wiser.messages.get(0)
        assert message1.envelopeSender == "postmaster@board.steffen-petz.de"
        assert message1.envelopeReceiver == "user1@board.com"
        assert message1.mimeMessage.subject == "New Note on Board"
        assert message1.mimeMessage.isMimeType("text/html")

        def message2 = wiser.messages.get(1)
        assert message2.envelopeSender == "postmaster@board.steffen-petz.de"
        assert message2.envelopeReceiver == "user1@board.com"
        assert message2.mimeMessage.subject == "New Note on Board"

        def message3 = wiser.messages.get(2)
        assert message3.envelopeSender == "postmaster@board.steffen-petz.de"
        assert message3.envelopeReceiver == "user2@board.com"
        assert message3.mimeMessage.subject == "New Note on Board"


        noteCommands.postNote(userId2, new NoteIn("message2.1", null))

        Awaitility.await().until { wiser.messages.size()>=4 }
        assert wiser.messages.size()==4

        def message21 = wiser.messages.get(3)
        assert message21.envelopeSender == "postmaster@board.steffen-petz.de"
        assert message21.envelopeReceiver == "user1@board.com"
        assert message21.mimeMessage.subject == "New Note on Board"
    }

    @Test
    void followeePostedNote_sendEmailFailure() {

        wiser.stop()

        def userId1 = userCommands.registerUser(new UserIn("user1", "user1@board.com", null, List.of()))
        def userId2 = userCommands.registerUser(new UserIn("user2", "user2@board.com", null, List.of()))
        followCommands.follow(userId1, userId2)

        noteCommands.postNote(userId2, new NoteIn("message2", null))

        // give asynchronous email sending some time
        sleep(1000)

        // check email failure did not cause the transaction to be rolled back
        assert followQueries.queryFolloweeByUserAndFolloweeUserId(userId1, userId2).newNoteAvailable()
    }
}
