package board.port.provided.user

import board.domain.note.Media
import board.port.provided.AbstractResourceTest
import groovy.transform.CompileStatic
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.skyscreamer.jsonassert.JSONCompareMode

import javax.ws.rs.core.HttpHeaders
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import java.net.http.HttpRequest

@QuarkusTest
@CompileStatic
class UserNotesResourceTest extends AbstractResourceTest
{
    @Test
    void getNotes_2()
    {
        def user1 = storeUser("user-1")
        def notes = (1..2).collect { postNote(user1, "message $it") }

        // notes must be readable by other users
        def user2 = storeUser("user-2")
        restClient.setAuthToken(user2.id(), 'ROLE_USER')

        def response = restClient.get("/api/users/${user1.id()}/notes")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "instant": "${notes[0].instant()}",
              "text": "message 1",
              "number": 1
            },
            {
              "instant": "${notes[1].instant()}",
              "text": "message 2",
              "number": 2
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getNotes_fromNumber()
    {
        def user1 = storeUser("user-1")
        def notes = (1..5).collect { postNote(user1, "message $it") }

        def response = restClient.get("/api/users/${user1.id()}/notes?fromNumber=4&page=0")

        JSONAssert.assertEquals("""
        {
          "content": [
            {
              "instant": "${notes[3].instant()}",
              "text": "message 4",
              "number": 4
            },
            {
              "instant": "${notes[4].instant()}",
              "text": "message 5",
              "number": 5
            }
          ],
          "number": 0,
          "size": 100,
          "totalElements": 2,
          "totalPages": 1
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void getNote()
    {
        def user1 = storeUser("user-1")
        def media = new Media("image/png", "01234".bytes)
        def notes = (1..2).collect { postNote(user1, "message $it", media) }

        def response = restClient.get("/api/users/${user1.id()}/notes/1")

        JSONAssert.assertEquals("""
        {
          "instant": "${notes[0].instant()}",
          "text": "message 1",
          "number": 1,
          "media": [
            {
              "mediaType": "${media.mediaType()}",
              "content": "${media.content().encodeBase64()}"
            }
          ]
        }""", response.body(), JSONCompareMode.STRICT)
    }

    @Test
    void deleteNote()
    {
        def user1 = storeUser("user-1")
        (1..2).forEach { postNote(user1, "message $it") }

        def noteUri = "/api/users/${user1.id()}/notes/1"

        // check note is present
        restClient.get(noteUri)

        def response = restClient.delete(noteUri)

        assert response.statusCode()== Response.Status.NO_CONTENT.statusCode
        assert restClient.exchange(HttpRequest.newBuilder(baseUri.resolve(noteUri)).build()).statusCode() == Response.Status.NOT_FOUND.statusCode
    }

    @Test
    void deleteNote_nonExistent()
    {
        def user1 = storeUser("user-1")

        def noteUri = "/api/users/${user1.id()}/notes/1"

        // deleting non existent note must not fail
        def response = restClient.delete(noteUri)

        assert response.statusCode()== Response.Status.NO_CONTENT.statusCode
    }

    @Test
    void postNote()
    {
        def request = """
        {
          "text": "message 1"
        }"""

        def user1 = storeUser("user-1")
        def notesUri = "/api/users/${user1.id()}/notes"

        restClient.setAuthToken(user1.id(), 'ROLE_USER')

        def response = restClient.post(notesUri, request)

        assert response.statusCode() == Response.Status.CREATED.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == null
        assert response.headers().firstValue(HttpHeaders.LOCATION).orElse(null) == baseUri.resolve("$notesUri/1").toString()
        assert response.body() == ''
    }

    @Test
    void postNote_limitExceeded()
    {
        def request = """
        {
          "text": "message 1"
        }"""

        def user1 = storeUser("user-1")
        def notesUri = "/api/users/${user1.id()}/notes"
        (1..5).forEach { restClient.post(notesUri, request) }

        def response = restClient.exchange(HttpRequest
                .newBuilder(baseUri.resolve(notesUri))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .POST(HttpRequest.BodyPublishers.ofString(request))
                .build())

        assert response.statusCode() == Response.Status.BAD_REQUEST.statusCode
        assert response.headers().firstValue(HttpHeaders.CONTENT_TYPE).orElse(null) == MediaType.APPLICATION_JSON

        JSONAssert.assertEquals("""
        {
          "message": "Number of notes limit of 5 exceeded",
          "path": "/api/users/${user1.id()}/notes"
        }""", (String)response.body(), JSONCompareMode.STRICT)
    }
}
