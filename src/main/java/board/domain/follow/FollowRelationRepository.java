package board.domain.follow;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public abstract class FollowRelationRepository
{
    protected abstract Optional<FollowRelation> findById(@Valid FollowRelation.Id id);

    protected abstract void add(@Valid FollowRelation followRelation);

    protected abstract void update(@Valid FollowRelation followRelation);

    protected abstract void deleteById(@Valid FollowRelation.Id id);

    protected abstract Stream<FollowRelation> getByFollowee(@NotNull UUID followee);

    protected abstract Stream<FollowRelation> getByFollower(@NotNull UUID follower);
}