package board.domain.user;

import lombok.RequiredArgsConstructor;

import javax.enterprise.event.Event;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Singleton
@RequiredArgsConstructor
public class UserRepoService
{
    private final UserRepository userRepository;
    private final Event<Object> eventService;

    public User getById(UUID id)
    {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(id));
    }

    @Transactional
    public void add(User user)
    {
        user.setUpdatedInstant(Instant.now());
        userRepository.add(user);
        eventService.fire(new UserAddedEvent(user));
        user.getEventQueue().takeEvents();  // drop events created before saving
    }

    @Transactional
    public void update(User user)
    {
        Instant now = Instant.now();
        int newRevisionNumber = user.getRevisionNumber()+1;
        userRepository.update(newRevisionNumber, now, user);
        user.setRevisionNumber(newRevisionNumber);
        user.setUpdatedInstant(now);
        publishEvents(user);
    }

    @Transactional
    public void delete(UUID userId)
    {
        eventService.fire(new BeforeUserDeletedEvent(userId));
        userRepository.deleteById(userId);
    }

    public Optional<String> findNotificationEmailContact(UUID userId)
    {
        return userRepository.findNotificationEmailAddress(userId);
    }

    private void publishEvents(User user)
    {
        for (Object event : user.getEventQueue().takeEvents())
            eventService.fire(event);
    }
}
