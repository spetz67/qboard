package board.domain.follow;

import board.domain.note.Note;

import javax.validation.constraints.NotNull;
import java.util.List;

public record FolloweePostedNoteEvent(

        @NotNull
        List<FollowRelation> followRelations,

        @NotNull
        Note note
)
{
}
