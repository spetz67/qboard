package board.port.provided


import board.application.follow.FollowCommands
import board.application.follow.FollowQueryRepository
import board.application.follow.FolloweeOut
import board.application.note.NoteCommands
import board.application.note.NoteIn
import board.application.note.NoteOut
import board.application.note.NoteQueryRepository
import board.application.user.UserCommands
import board.application.user.UserIn
import board.application.user.UserOut
import board.application.user.UserQueryRepository
import board.domain.note.Media
import board.domain.user.PhoneContact
import board.port.required.TestRepository
import groovy.transform.CompileStatic
import io.quarkus.test.common.http.TestHTTPResource
import org.junit.jupiter.api.BeforeEach

import javax.inject.Inject

@CompileStatic
abstract class AbstractResourceTest
{
    @Inject
    RestClient restClient

    @Inject
    TestRepository testRepository

    @Inject
    UserQueryRepository userQueries

    @Inject
    NoteQueryRepository noteQueries

    @Inject
    FollowQueryRepository followQueries

    @Inject
    UserCommands userApp

    @Inject
    NoteCommands noteApp

    @Inject
    FollowCommands followApp

    @TestHTTPResource
    URI baseUri

    @BeforeEach
    void setup()
    {
        restClient.setAuthToken(UUID.randomUUID(), 'ROLE_USER', 'ROLE_ADMIN')

        testRepository.deleteAll()
    }

    // convenience methods to store resources and query database to get the instant with stored time precision

    UserOut storeUser(String name, PhoneContact... phoneContacts)
    {
        def userIn = new UserIn(name, null, List.of(phoneContacts), null)
        return storeUser(userIn)
    }

    UserOut storeUser(UserIn userIn)
    {
        def userId = userApp.registerUser(userIn)
        return new UserOut(userId, null, 0, 0, userIn.name(), userIn.notificationEmailAddress(), null, null)
    }

    NoteOut postNote(UserOut user, String text, Media... media)
    {
        def note = noteApp.postNote(user.id(), new NoteIn(text, media.length>0 ? List.of(media) : null))
        return noteQueries.queryNoteByUserIdAndNumber(note.userId(), note.number())
    }

    FolloweeOut follow(UserOut user, UUID followeeUserId)
    {
        followApp.follow(user.id(), followeeUserId)
        return followQueries.queryFolloweeByUserAndFolloweeUserId(user.id(), followeeUserId)
    }
}
