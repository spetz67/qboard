-- Add dedicated user email address for notification

drop view "user_view";

alter table "user"
    add column "notification_email_address" varchar(60);

-- populate the new column with the first found address from email contacts
update "user"
    set "notification_email_address" =
        (select "email_address" from "user_email_contact" where "user_id"="user"."id" limit 1)
    where true;

create view "user_view" as
select *,
       (select count(*) from "follow_relation" where "followee_user_id" = user_view."id")::int as follower_count,
       (select coalesce(json_agg(json_build_object('type', "type", 'emailAddress', "email_address")), json_build_array())
        from "user_email_contact"
        where "user_id" = user_view."id") as email_contacts_json
from "user" user_view
