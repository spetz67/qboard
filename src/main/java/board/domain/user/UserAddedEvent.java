package board.domain.user;

public record UserAddedEvent(User user)
{
}
